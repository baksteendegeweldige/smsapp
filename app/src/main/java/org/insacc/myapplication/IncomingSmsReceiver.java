package org.insacc.myapplication;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.content.WakefulBroadcastReceiver;
import android.telephony.SmsManager;
import android.telephony.SmsMessage;
import android.util.Log;

import org.insacc.myapplication.Service.ForwardSmsService.ForwardSmsServiceImp;

/**
 * Created by can on 22.01.2017.
 */

public class IncomingSmsReceiver extends WakefulBroadcastReceiver {


    @Override
    public void onReceive(Context context, Intent intent) {

        // Niet activeren
        /*
        Log.i("incomingSmsReceiver", "called");

        Bundle bundle = intent.getExtras();

        SmsMessage[] smsMessages;

        String smsToSend = "";

        if (bundle != null) {
            // Retrieve the SMS Messages received
            Object[] receivedSms = (Object[]) bundle.get("pdus");
            String format = intent.getStringExtra("format");

            smsMessages = new SmsMessage[receivedSms.length];

            // For every SMS message received
            for (int i = 0; i < smsMessages.length; i++) {
                // Convert Object array
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    smsMessages[i] = SmsMessage.createFromPdu((byte[]) receivedSms[i], format);
                } else {
                    smsMessages[i] = SmsMessage.createFromPdu((byte[]) receivedSms[i]);
                }
                // Sender's phone number
                smsToSend += "SMS from " + smsMessages[i].getOriginatingAddress() + " : ";
                // Fetch the text message

                smsToSend += smsMessages[i].getMessageBody().toString();
                smsToSend += "\n";
                Intent intentForwardService = new Intent(context, ForwardSmsServiceImp.class);
                intentForwardService.putExtra(Config.RECEIVED_SMS_NUMBER, smsMessages[i].getOriginatingAddress());
                intentForwardService.putExtra(Config.RECEIVED_SMS_TEXT, smsMessages[i].getMessageBody().toString());
                startWakefulService(context, intentForwardService);

            }
            Log.d("Test Sms", smsToSend);

        }*/
    }
}
