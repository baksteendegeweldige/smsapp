package org.insacc.myapplication.Database.AddSmsRuleService;

import android.util.Log;

import org.insacc.myapplication.Model.Booking;
import org.insacc.myapplication.Model.BookingSkuItem;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by can on 3.02.2017.
 */

public class SkuItemConverter {

    public SkuItemConverter() {

    }

    public String covertListToDb(List<BookingSkuItem> skuItems) {
        StringBuilder stringBuilder = new StringBuilder();

        if(skuItems != null && skuItems.size() == 0) {
            Log.i("skuItemSize", String.valueOf(skuItems.size()));
            return null;
        }
        if (skuItems != null) {
            for (int i = 0; i < skuItems.size(); i++) {
                stringBuilder.append(skuItems.get(i).convertToDbItem());
                if (i != skuItems.size() - 1)
                    stringBuilder.append(" ");
            }

        }


        return stringBuilder.toString();

    }

    public List<BookingSkuItem> convertDbToSkuList(String skuItemFromDb) {
        List<BookingSkuItem> bookingSkuItems = new ArrayList<>();
        if (skuItemFromDb != null) {
            String[] orItems = skuItemFromDb.split("\\s");

            for (int i = 0; i < orItems.length; i++) {
                String[] andItems = orItems[i].split(",");
                List<String> itemNames = new ArrayList<>();
                for (int k = 0; k < andItems.length; k++) {
                    itemNames.add(andItems[k]);

                }

                BookingSkuItem bookingSkuItem = new BookingSkuItem(itemNames);
                bookingSkuItems.add(bookingSkuItem);
            }


        } else {
            return null;
        }
        return bookingSkuItems;
    }
}
