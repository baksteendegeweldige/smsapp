package org.insacc.myapplication.Database.CheckCustInfoService;

import org.insacc.myapplication.Database.BaseService;
import org.insacc.myapplication.Model.CustomerInformation;

/**
 * Created by can on 15.02.2017.
 */

public interface CheckCustInfoService extends BaseService{


    void checkCustomerInfo(String phoneNumber, CheckCustInfoCallback callback);

    interface CheckCustInfoCallback {

        void onCustomerInfoLoaded(CustomerInformation customerInformation);

        void onCustomerInfoNotFound();
    }

}
