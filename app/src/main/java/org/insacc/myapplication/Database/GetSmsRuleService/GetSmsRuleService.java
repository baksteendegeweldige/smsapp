package org.insacc.myapplication.Database.GetSmsRuleService;

import org.insacc.myapplication.Database.BaseService;
import org.insacc.myapplication.Model.SmsRule;

/**
 * Created by can on 31.01.2017.
 */

public interface GetSmsRuleService extends BaseService {

    void getSmsRule(long smsRuleId, GetSmsRuleCallback callback);

    interface GetSmsRuleCallback {
        void onSmsRuleLoaded(SmsRule smsRule);

        void onSmsRuleLoadFailed();
    }
}
