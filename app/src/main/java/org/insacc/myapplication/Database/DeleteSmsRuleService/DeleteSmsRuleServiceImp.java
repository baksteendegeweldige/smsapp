package org.insacc.myapplication.Database.DeleteSmsRuleService;

import android.database.sqlite.SQLiteDatabase;

import org.insacc.myapplication.Database.DatabaseHelper;

import java.util.concurrent.Callable;

import rx.Single;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

/**
 * Created by can on 15.02.2017.
 */

public class DeleteSmsRuleServiceImp implements DeleteSmsRuleService {

    private Subscription mSubscription;

    private DatabaseHelper mDbHelper;


    public DeleteSmsRuleServiceImp(DatabaseHelper databaseHelper) {
        this.mDbHelper = databaseHelper;
    }


    @Override
    public void deleteSmsRule(final long smsRuleId, final SmsRuleDeleteCallback callback) {


        Single<Long> deleteSmsRule = Single.fromCallable(new Callable<Long>() {
            @Override
            public Long call() throws Exception {
                return deleteSmsRuleHelper(smsRuleId);
            }
        })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.computation());

        mSubscription = deleteSmsRule.subscribe(new Action1<Long>() {
            @Override
            public void call(Long isDeletedId) {
                if (isDeletedId != -1) {
                    callback.onSmsRuleDeleted(isDeletedId);
                } else {
                    callback.onSmsRuleDeleteFail();
                }
            }
        });

    }

    private long deleteSmsRuleHelper(long smsRuleId) {
        SQLiteDatabase database = mDbHelper.getWritableDatabase();


        String deleteQuery = "delete from " + DatabaseHelper.SMS_RULE_TABLE + " where "
                + " rowid =" + smsRuleId;

        database.execSQL(deleteQuery);

        database.close();
        //TODO change delete execution;
        return smsRuleId;


    }

    @Override
    public void unSubscribe() {
        if (mSubscription != null && !mSubscription.isUnsubscribed())
            mSubscription.unsubscribe();
    }
}
