package org.insacc.myapplication.Database.SentSmsDataService;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import org.insacc.myapplication.Database.DatabaseHelper;
import org.insacc.myapplication.Model.CustomerInformation;

import java.util.concurrent.Callable;

import rx.Single;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

/**
 * Created by can on 22.01.2017.
 */

public class SentSmsDataServiceImp implements SentSmsDataService {

    private DatabaseHelper mDatabaseHelper;

    private Subscription mGetSubscription;
    private Subscription mAddSubscription;

    public SentSmsDataServiceImp(DatabaseHelper databaseHelper) {
        this.mDatabaseHelper = databaseHelper;
    }

    @Override
    public void unSubscribe() {
        if (mGetSubscription != null && !mGetSubscription.isUnsubscribed())
            mGetSubscription.unsubscribe();
    }

    @Override
    public void saveCustomerInformation(final CustomerInformation customerInformation, final SaveCustomerInfoCallback callback) {
        Single<Boolean> saveInfo = Single.fromCallable(new Callable<Boolean>() {
            @Override
            public Boolean call() throws Exception {
                return saveCustomerInfoHelper(customerInformation);
            }
        })
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread());

        mAddSubscription = saveInfo.subscribe(new Action1<Boolean>() {
            @Override
            public void call(Boolean aBoolean) {
                if (aBoolean) {
                    callback.onCustomerInfoSave();
                } else {
                    callback.onCustomerInfoSaveFail();
                }
            }
        });
    }

    private boolean saveCustomerInfoHelper(CustomerInformation customerInformation) {
        SQLiteDatabase database = mDatabaseHelper.getWritableDatabase();

        ContentValues customerValue = new ContentValues();
        customerValue.put(DatabaseHelper.CUSTOMER_NAME, customerInformation.getCustomerName());
        customerValue.put(DatabaseHelper.CUSTOMER_PHONE, customerInformation.getPhoneNumber());
        customerValue.put(DatabaseHelper.CUSTOMER_RESERVATION_NUMBER, customerInformation.getReservationID());

        long result = database.insertWithOnConflict(DatabaseHelper.CUSTOMER_PHONE_TABLE, null, customerValue,
                SQLiteDatabase.CONFLICT_REPLACE);

        database.close();
        return result != -1;
    }

    @Override
    public void getCustomerInformation(final String customerName, final GetCustomerInfoCallback getCustomerInfoCallback) {
        final Single<CustomerInformation> getCustomerInfo = Single.fromCallable(new Callable<CustomerInformation>() {
            @Override
            public CustomerInformation call() throws Exception {
                return getCustomerInfoHelper(customerName);
            }
        })
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread());

        mGetSubscription = getCustomerInfo.subscribe(new Action1<CustomerInformation>() {
            @Override
            public void call(CustomerInformation customerInformation) {
                if (customerInformation.getCustomerName() != null) {
                    getCustomerInfoCallback.onCustomerInfoLoaded(customerInformation);
                } else {
                    getCustomerInfoCallback.onCustomerInfoLoadFail();
                }
            }
        });
    }

    private CustomerInformation getCustomerInfoHelper(String customerName) {

        SQLiteDatabase database = mDatabaseHelper.getReadableDatabase();
        CustomerInformation customerInformation = new CustomerInformation();
        String selectQuery = "SELECT * FROM " + DatabaseHelper.CUSTOMER_PHONE_TABLE
                + " WHERE " + DatabaseHelper.CUSTOMER_NAME + "='" + customerName + "'";

        Cursor cursor = database.rawQuery(selectQuery, null);


        try {
            if (cursor.moveToFirst()) {

                Log.i("customerFound", "called");
                String customerPhone = cursor.getString(cursor.getColumnIndex(DatabaseHelper.CUSTOMER_PHONE));
                customerInformation.setCustomerName(customerName);
                customerInformation.setPhoneNumber(customerPhone);


            }
        } catch (Exception e) {
            //  Log.d(TAG, "Error while trying to get posts from database");

        } finally {

            if (cursor != null && !cursor.isClosed()) {
                cursor.close();
            }
            database.close();
        }

        return customerInformation;
    }


}
