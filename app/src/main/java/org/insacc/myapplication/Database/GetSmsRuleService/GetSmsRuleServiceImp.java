package org.insacc.myapplication.Database.GetSmsRuleService;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import org.insacc.myapplication.Database.DatabaseHelper;
import org.insacc.myapplication.Model.BookingSkuItem;
import org.insacc.myapplication.Model.SmsRule;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

import rx.Single;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

/**
 * Created by can on 31.01.2017.
 */

public class GetSmsRuleServiceImp implements GetSmsRuleService {

    private Subscription mSubscription;

    private DatabaseHelper mDbHelper;

    public GetSmsRuleServiceImp(DatabaseHelper databaseHelper) {
        mDbHelper = databaseHelper;
    }

    private SmsRule getSmsRuleHelper(long smsRuleId) {
        SQLiteDatabase database = mDbHelper.getReadableDatabase();

        String selectQuery = "SELECT * FROM " + DatabaseHelper.SMS_RULE_TABLE + " WHERE ROWID="
                + smsRuleId;

        SmsRule smsRule = new SmsRule();

        Cursor cursor = null;
        try {
            if (!database.isOpen())
            {
                database = mDbHelper.getReadableDatabase();
            }
            cursor = database.rawQuery(selectQuery, null);
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            if (cursor.moveToFirst()) {
                do {

                    Log.i("bookingItem", "called");
                    //statuses.add(cursor.getString(cursor.getColumnIndex(
                    //      DatabaseHelper.BOOKING_STATUS_NAME)));
                    smsRule.setmCurrentHour(cursor.getString(cursor.getColumnIndex(DatabaseHelper
                            .SMS_RULE_CURR_HOUR)));

                    smsRule.setmCurrentMinute(cursor.getString(cursor.getColumnIndex(DatabaseHelper
                            .SMS_RULE_CURR_MINUTE)));

                    smsRule.setCheckInOutStatus(cursor.getInt(cursor.getColumnIndex(DatabaseHelper
                            .SMS_RULE_CHECK_IN_STATUS)));

                    smsRule.setAdditionalDayCount(cursor.getInt(cursor.getColumnIndex(DatabaseHelper
                            .SMS_RULE_ADD_DAY)));

                    smsRule.setStartDayToday(cursor.getInt(cursor.getColumnIndex(DatabaseHelper
                            .SMS_RULE_IS_START_TODAY)) == 1);

                   // smsRule.setReceivedSms(cursor.getInt(cursor.getColumnIndex(DatabaseHelper
                     //       .SMS_RULE_IS_SMS_RECEIVED)) == 1);

                    smsRule.setRuleName(cursor.getString(cursor.getColumnIndex(DatabaseHelper.SMS_RULE_NAME)));

                    smsRule.setmBookingStatus(cursor.getString(cursor.getColumnIndex(DatabaseHelper
                            .SMS_RULE_BOOKING_STATUS)));

                    smsRule.setSmsToSend(cursor.getString(cursor.getColumnIndex(DatabaseHelper
                            .SMS_RULE_SMS_TEXT)));

                    smsRule.setId(smsRuleId);
                    Log.i("testSmsRule", "called1");
                    smsRule.setBookingContainItems(extractBookingSkuItems(cursor.getString(cursor
                            .getColumnIndex(DatabaseHelper.SMS_RULE_BOOKING_CONTAIN))));

                    smsRule.setBookingNotContainItems(extractBookingSkuItems(cursor.getString(cursor
                            .getColumnIndex(DatabaseHelper.SMS_RULE_BOOKING_NOT_CONTAIN))));


                } while (cursor.moveToNext());
            }
        } catch (Exception e) {


        } finally {

            if (cursor != null && !cursor.isClosed()) {
                cursor.close();
            }
            database.close();
        }

        return smsRule;

    }

    private List<BookingSkuItem> extractBookingSkuItems(String items) {
        List<BookingSkuItem> skuItems = new ArrayList<>();
        if(items == null)
            return null;
        String[] orItems = items.split("\\s+");

        for (int i = 0; i < orItems.length; i++) {
            List<String> tempItems = new ArrayList<>();
            String[] andItems = orItems[i].split(",");
            for (int k = 0; k < andItems.length; k++) {
                tempItems.add(andItems[k]);
            }
            BookingSkuItem tempSkuItem = new BookingSkuItem(tempItems);
            skuItems.add(tempSkuItem);
        }

        return skuItems;
    }

    @Override
    public void getSmsRule(final long smsRuleId, final GetSmsRuleCallback callback) {
        Single<SmsRule> getSmsRule = Single.fromCallable(new Callable<SmsRule>() {
            @Override
            public SmsRule call() throws Exception {
                return getSmsRuleHelper(smsRuleId);
            }
        })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.computation());

        mSubscription = getSmsRule.subscribe(new Action1<SmsRule>() {
            @Override
            public void call(SmsRule smsRule) {
                if (smsRule != null) {
                    callback.onSmsRuleLoaded(smsRule);
                } else {
                    callback.onSmsRuleLoadFailed();
                }
            }
        });
    }

    @Override
    public void unSubscribe() {
        if (mSubscription != null && !mSubscription.isUnsubscribed())
            mSubscription.unsubscribe();
    }
}
