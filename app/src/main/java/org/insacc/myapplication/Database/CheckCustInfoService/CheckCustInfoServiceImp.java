package org.insacc.myapplication.Database.CheckCustInfoService;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import org.insacc.myapplication.Database.DatabaseHelper;
import org.insacc.myapplication.Model.CustomerInformation;

import java.util.concurrent.Callable;

import rx.Single;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

/**
 * Created by can on 15.02.2017.
 */

public class CheckCustInfoServiceImp implements CheckCustInfoService {

    private DatabaseHelper mDbHelper;

    private Subscription mSubscription;

    public CheckCustInfoServiceImp(DatabaseHelper databaseHelper) {
        this.mDbHelper = databaseHelper;
    }


    @Override
    public void checkCustomerInfo(final String phoneNumber, final CheckCustInfoCallback callback) {
        Log.i("checkInfo", "called3");
        Single<CustomerInformation> getCustomerInfo = Single.fromCallable(new Callable<CustomerInformation>() {
            @Override
            public CustomerInformation call() throws Exception {
                return checkCustomerInfoHelper(phoneNumber);
            }
        })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.computation());

        mSubscription = getCustomerInfo.subscribe(new Action1<CustomerInformation>() {
            @Override
            public void call(CustomerInformation customerInformation) {
                if (customerInformation != null) {
                    callback.onCustomerInfoLoaded(customerInformation);
                } else {
                    callback.onCustomerInfoNotFound();
                }
            }
        });
    }


    private CustomerInformation checkCustomerInfoHelper(String phoneNumber) {
        SQLiteDatabase database = mDbHelper.getReadableDatabase();

        String selectQuery = "select " + DatabaseHelper.CUSTOMER_PHONE + ", " + DatabaseHelper.CUSTOMER_NAME
                + ", rowid " + "from " + DatabaseHelper.CUSTOMER_PHONE_TABLE
                + " where " + DatabaseHelper.CUSTOMER_PHONE + "='" + phoneNumber + "'";


        Cursor cursor = database.rawQuery(selectQuery, null);
        CustomerInformation customerInformation = null;
        Log.i("checkInfo", "called");
        try {
            if (cursor.moveToFirst()) {
                do {
                    customerInformation = new CustomerInformation();
                    Log.i("checkInfo", "called1");
                    customerInformation.setCustomerName(cursor.getString(cursor.getColumnIndex(
                            DatabaseHelper.CUSTOMER_NAME)));

                    customerInformation.setPhoneNumber(phoneNumber);
                    customerInformation.setReservationID(cursor.getLong(cursor.getColumnIndex("rowid")));
                    Log.i("checkInfo", "called2");

                } while (cursor.moveToNext());
            }
        } catch (Exception e) {


        } finally {

            if (cursor != null && !cursor.isClosed()) {
                cursor.close();
            }
            database.close();
        }

        return customerInformation;
    }

    @Override
    public void unSubscribe() {
        if (mSubscription != null && !mSubscription.isUnsubscribed())
            mSubscription.unsubscribe();
    }
}
