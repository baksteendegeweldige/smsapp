package org.insacc.myapplication.Database.BookingItemService;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import org.insacc.myapplication.Database.BookingStatusService.BookingStatusServiceImp;
import org.insacc.myapplication.Database.DatabaseHelper;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

import rx.Single;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

/**
 * Created by can on 27.01.2017.
 */

public class BookingItemServiceImp implements BookingItemService {

    private DatabaseHelper mDbHelper;

    private Subscription mSubscription;

    public BookingItemServiceImp(DatabaseHelper databaseHelper) {
        this.mDbHelper = databaseHelper;
    }

    @Override
    public void getBookingItems(final GetBookingItemsCallback callback) {
        Single<List<String>> getBookingItems = Single.fromCallable(new Callable<List<String>>() {
            @Override
            public List<String> call() throws Exception {
                return getBookingItemsHelper();
            }

        }).subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread());

        mSubscription = getBookingItems.subscribe(new Action1<List<String>>() {
            @Override
            public void call(List<String> bookingItems) {
                if (bookingItems != null && bookingItems.size() > 0) {

                    callback.onBookingItemsLoaded(bookingItems);
                } else {
                    callback.onBookingItemLoadFail();
                }
            }
        });
    }

    private List<String> getBookingItemsHelper() {
        SQLiteDatabase database = mDbHelper.getReadableDatabase();

        String selectQuery = "SELECT * FROM " + DatabaseHelper.BOOKINGS_ITEMS_TABLE;


        Cursor cursor = database.rawQuery(selectQuery, null);
        List<String> bookingItems = new ArrayList<>(0);
        try {
            if (cursor.moveToFirst()) {
                do {

                    Log.i("bookingItem", "called");
                    bookingItems.add(cursor.getString(cursor.getColumnIndex(
                            DatabaseHelper.BOOKING_SKU_ITEM_CODE)));


                } while (cursor.moveToNext());
            }
        } catch (Exception e) {


        } finally {

            if (cursor != null && !cursor.isClosed()) {
                cursor.close();
            }
            database.close();
        }


        return bookingItems;
    }

    @Override
    public void unSubscribe() {
        if (mSubscription != null && !mSubscription.isUnsubscribed())
            mSubscription.unsubscribe();
    }
}
