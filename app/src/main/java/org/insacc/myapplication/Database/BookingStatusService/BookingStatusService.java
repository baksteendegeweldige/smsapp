package org.insacc.myapplication.Database.BookingStatusService;

import org.insacc.myapplication.Database.BaseService;
import org.insacc.myapplication.Model.BookingStatus;

import java.util.List;

/**
 * Created by can on 27.01.2017.
 */

public interface BookingStatusService extends BaseService {

    void getAllBookingStatuses(GetAllStatusesCallback callback);

    void getBookingStatus(String bookingStatus, GetBookingStatusCallback callback);

    void saveBookingStatus(String oldBookingStatus, String newBookingStatus, SaveStatusCallback callback);

    interface GetAllStatusesCallback {
        void onStatusesLoaded(List<String> statuses);

        void onStatusLoadFail();
    }

    interface GetBookingStatusCallback {
        void onBookingStatusLoaded(String bookingStatus);

        void onBookingStatusLoadFail();
    }

    interface SaveStatusCallback {
        void onBookingStatusSaved();

        void onBookingStatusSaveFail();
    }
}
