package org.insacc.myapplication.Database.AddSmsRuleService;

import org.insacc.myapplication.Database.BaseService;
import org.insacc.myapplication.Model.SmsRule;

/**
 * Created by can on 15.01.2017.
 */

public interface AddSmsService extends BaseService {


    void addSmsRule(SmsRule smsRule, AddSmsRuleCallback callback);

    void updateSmsRule(SmsRule smsRule, UpdateSmsRuleCallback callback);

    interface AddSmsRuleCallback {
        void onSmsRuleAdded(long smsRuleId);

        void onSmsRuleAddFailed();
    }

    interface UpdateSmsRuleCallback {

        void onSmsRuleUpdated();

        void onSmsRuleUpdateFail();
    }
}
