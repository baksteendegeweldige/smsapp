package org.insacc.myapplication.Database.GetSmsRulesService;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import org.insacc.myapplication.Database.DatabaseHelper;
import org.insacc.myapplication.Model.BookingSkuItem;
import org.insacc.myapplication.Model.SmsRule;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

import rx.Single;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

/**
 * Created by can on 12.02.2017.
 */

public class GetSmsRulesServiceImp implements GetSmsRulesService {

    private DatabaseHelper mDbHelper;

    private Subscription mSubscription;

    public GetSmsRulesServiceImp(DatabaseHelper databaseHelper) {
        this.mDbHelper = databaseHelper;
    }


    private List<SmsRule> getSmsRulesHelper() {
        SQLiteDatabase database = mDbHelper.getReadableDatabase();

        String selectQuery = "SELECT " + "rowid, " + DatabaseHelper.SMS_RULE_ADD_DAY + ","
                + DatabaseHelper.SMS_RULE_CURR_HOUR + "," + DatabaseHelper.SMS_RULE_CURR_MINUTE + ","
                + DatabaseHelper.SMS_RULE_CHECK_IN_STATUS + "," + DatabaseHelper.SMS_RULE_IS_START_TODAY
                + ", " + DatabaseHelper.SMS_RULE_NAME + "," + DatabaseHelper.SMS_RULE_BOOKING_STATUS
                + "," + DatabaseHelper.SMS_RULE_SMS_TEXT + "," + DatabaseHelper.SMS_RULE_BOOKING_CONTAIN
                + "," + DatabaseHelper.SMS_RULE_BOOKING_NOT_CONTAIN
                + " FROM " + DatabaseHelper.SMS_RULE_TABLE;
        List<SmsRule> smsRules = new ArrayList<>();


        Cursor cursor = database.rawQuery(selectQuery, null);

        try {
            if (cursor.moveToFirst()) {
                do {
                    SmsRule smsRule = new SmsRule();

                    //statuses.add(cursor.getString(cursor.getColumnIndex(
                    //      DatabaseHelper.BOOKING_STATUS_NAME)));
                    smsRule.setmCurrentHour(cursor.getString(cursor.getColumnIndex(DatabaseHelper
                            .SMS_RULE_CURR_HOUR)));

                    smsRule.setmCurrentMinute(cursor.getString(cursor.getColumnIndex(DatabaseHelper
                            .SMS_RULE_CURR_MINUTE)));

                    smsRule.setCheckInOutStatus(cursor.getInt(cursor.getColumnIndex(DatabaseHelper
                            .SMS_RULE_CHECK_IN_STATUS)));

                    smsRule.setAdditionalDayCount(cursor.getInt(cursor.getColumnIndex(DatabaseHelper
                            .SMS_RULE_ADD_DAY)));

                    smsRule.setStartDayToday(cursor.getInt(cursor.getColumnIndex(DatabaseHelper
                            .SMS_RULE_IS_START_TODAY)) == 1);

                    // smsRule.setReceivedSms(cursor.getInt(cursor.getColumnIndex(DatabaseHelper
                    //       .SMS_RULE_IS_SMS_RECEIVED)) == 1);

                    smsRule.setRuleName(cursor.getString(cursor.getColumnIndex(DatabaseHelper.SMS_RULE_NAME)));

                    smsRule.setmBookingStatus(cursor.getString(cursor.getColumnIndex(DatabaseHelper
                            .SMS_RULE_BOOKING_STATUS)));

                    smsRule.setSmsToSend(cursor.getString(cursor.getColumnIndex(DatabaseHelper
                            .SMS_RULE_SMS_TEXT)));

                    smsRule.setId(cursor.getInt(cursor.getColumnIndex("rowid")));
                    //Log.i("testSmsRule", "called1");
                    smsRule.setBookingContainItems(extractBookingSkuItems(cursor.getString(cursor
                            .getColumnIndex(DatabaseHelper.SMS_RULE_BOOKING_CONTAIN))));

                    smsRule.setBookingNotContainItems(extractBookingSkuItems(cursor.getString(cursor
                            .getColumnIndex(DatabaseHelper.SMS_RULE_BOOKING_NOT_CONTAIN))));

                    smsRules.add(smsRule);
                } while (cursor.moveToNext());
            }
        } catch (Exception e) {


        } finally {

            if (cursor != null && !cursor.isClosed()) {
                cursor.close();
            }
            database.close();
        }


        return smsRules;
    }

    private List<BookingSkuItem> extractBookingSkuItems(String items) {
        List<BookingSkuItem> skuItems = new ArrayList<>();
        if(items == null || items.length() == 0)
            return null;
        String[] orItems = items.split("\\s+");

        for (int i = 0; i < orItems.length; i++) {
            List<String> tempItems = new ArrayList<>();
            String[] andItems = orItems[i].split(",");
            for (int k = 0; k < andItems.length; k++) {
                tempItems.add(andItems[k]);
            }
            BookingSkuItem tempSkuItem = new BookingSkuItem(tempItems);
            skuItems.add(tempSkuItem);
        }

        return skuItems;
    }

    @Override
    public void getSmsRules(final GetSmsRulesCallback callback) {


        Single<List<SmsRule>> getRules = Single.fromCallable(new Callable<List<SmsRule>>() {
            @Override
            public List<SmsRule> call() throws Exception {

                return getSmsRulesHelper();
            }
        })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.computation());


        mSubscription = getRules.subscribe(new Action1<List<SmsRule>>() {
            @Override
            public void call(List<SmsRule> smsRules) {
                if (smsRules != null) {
                    callback.onSmsRulesLoaded(smsRules);
                } else {
                    callback.onSmsRuleLoadFailed();
                }

            }
        });
    }

    @Override
    public void unSubscribe() {
        if (mSubscription != null && !mSubscription.isUnsubscribed())
            mSubscription.unsubscribe();
    }
}
