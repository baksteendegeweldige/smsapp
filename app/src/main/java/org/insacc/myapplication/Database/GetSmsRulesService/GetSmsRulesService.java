package org.insacc.myapplication.Database.GetSmsRulesService;

import org.insacc.myapplication.Database.BaseService;
import org.insacc.myapplication.Model.SmsRule;

import java.util.List;

/**
 * Created by can on 12.02.2017.
 */

public interface GetSmsRulesService extends BaseService {

    void getSmsRules(GetSmsRulesCallback callback);

    interface GetSmsRulesCallback {

        void onSmsRulesLoaded(List<SmsRule> smsRuleList);

        void onSmsRuleLoadFailed();
    }
}
