package org.insacc.myapplication.Database.AddSmsRuleService;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;

import org.insacc.myapplication.Database.DatabaseHelper;
import org.insacc.myapplication.Model.SmsRule;

import java.util.concurrent.Callable;

import rx.Single;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

/**
 * Created by can on 15.01.2017.
 */

public class AddSmsServiceImp implements AddSmsService {

    private Subscription mSubscription;

    DatabaseHelper mDatabaseHelper;

    public AddSmsServiceImp(DatabaseHelper databaseHelper) {

        this.mDatabaseHelper = databaseHelper;
    }


    @Override
    public void addSmsRule(final SmsRule smsRule, final AddSmsRuleCallback callback) {
        final Single<Long> addSms = Single.fromCallable(new Callable<Long>() {
            @Override
            public Long call() throws Exception {
                return addSmsRuleHelper(smsRule);
            }
        }).subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread());

        mSubscription = addSms.subscribe(new Action1<Long>() {
            @Override
            public void call(Long isAdded) {
                if (isAdded != -1) {
                    callback.onSmsRuleAdded(isAdded);
                } else {
                    callback.onSmsRuleAddFailed();


                }
            }
        });
    }

    @Override
    public void updateSmsRule(final SmsRule smsRule, final UpdateSmsRuleCallback callback) {
        final Single<Boolean> addSms = Single.fromCallable(new Callable<Boolean>() {
            @Override
            public Boolean call() throws Exception {
                return isRuleUpdated(smsRule);
            }
        }).subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread());

        mSubscription = addSms.subscribe(new Action1<Boolean>() {
            @Override
            public void call(Boolean isAdded) {
                if (isAdded) {
                    callback.onSmsRuleUpdated();
                } else {
                    callback.onSmsRuleUpdateFail();


                }
            }
        });
    }

    private boolean isRuleUpdated(SmsRule smsRule) {
        SQLiteDatabase database = mDatabaseHelper.getWritableDatabase();

        ContentValues smsRuleToAdd = new ContentValues();
        smsRuleToAdd.put(DatabaseHelper.SMS_RULE_NAME, smsRule.getRuleName());
        smsRuleToAdd.put(DatabaseHelper.SMS_RULE_SMS_TEXT, smsRule.getSmsToSend());
        //smsRuleToAdd.put(DatabaseHelper.SMS_RULE_IS_SMS_RECEIVED, smsRule.isReceivedSms());
        smsRuleToAdd.put(DatabaseHelper.SMS_RULE_ADD_DAY, smsRule.getAdditionalDayCount());
        smsRuleToAdd.put(DatabaseHelper.SMS_RULE_CURR_HOUR, smsRule.getmCurrentHour());
        smsRuleToAdd.put(DatabaseHelper.SMS_RULE_CURR_MINUTE, smsRule.getmCurrentMinute());
        smsRuleToAdd.put(DatabaseHelper.SMS_RULE_IS_START_TODAY, smsRule.isStartDayToday());
        smsRuleToAdd.put(DatabaseHelper.SMS_RULE_CHECK_IN_STATUS, smsRule.getCheckInOutStatus());
        smsRuleToAdd.put(DatabaseHelper.SMS_RULE_BOOKING_STATUS, smsRule.getmBookingStatus());

        //TODO add smsrule attributes to the db
        SkuItemConverter skuItemConverter = new SkuItemConverter();

        smsRuleToAdd.put(DatabaseHelper.SMS_RULE_BOOKING_CONTAIN, skuItemConverter
                .covertListToDb(smsRule.getBookingContainItems()));

        smsRuleToAdd.put(DatabaseHelper.SMS_RULE_BOOKING_NOT_CONTAIN, skuItemConverter
                .covertListToDb(smsRule.getBookingNotContainItems()));

        long result = database.update(DatabaseHelper.SMS_RULE_TABLE, smsRuleToAdd,
                "rowid =?", new String[]{String.valueOf(smsRule.getId())});

        database.close();

        return result != -1;
    }

    private long addSmsRuleHelper(SmsRule smsRule) {
        SQLiteDatabase database = mDatabaseHelper.getWritableDatabase();

        ContentValues smsRuleToAdd = new ContentValues();
        smsRuleToAdd.put(DatabaseHelper.SMS_RULE_NAME, smsRule.getRuleName());
        smsRuleToAdd.put(DatabaseHelper.SMS_RULE_SMS_TEXT, smsRule.getSmsToSend());
        //smsRuleToAdd.put(DatabaseHelper.SMS_RULE_IS_SMS_RECEIVED, smsRule.isReceivedSms());
        smsRuleToAdd.put(DatabaseHelper.SMS_RULE_ADD_DAY, smsRule.getAdditionalDayCount());
        smsRuleToAdd.put(DatabaseHelper.SMS_RULE_CURR_HOUR, smsRule.getmCurrentHour());
        smsRuleToAdd.put(DatabaseHelper.SMS_RULE_CURR_MINUTE, smsRule.getmCurrentMinute());
        smsRuleToAdd.put(DatabaseHelper.SMS_RULE_IS_START_TODAY, smsRule.isStartDayToday());
        smsRuleToAdd.put(DatabaseHelper.SMS_RULE_CHECK_IN_STATUS, smsRule.getCheckInOutStatus());
        smsRuleToAdd.put(DatabaseHelper.SMS_RULE_BOOKING_STATUS, smsRule.getmBookingStatus());


        SkuItemConverter skuItemConverter = new SkuItemConverter();

        smsRuleToAdd.put(DatabaseHelper.SMS_RULE_BOOKING_CONTAIN, skuItemConverter
                .covertListToDb(smsRule.getBookingContainItems()));

        smsRuleToAdd.put(DatabaseHelper.SMS_RULE_BOOKING_NOT_CONTAIN, skuItemConverter
                .covertListToDb(smsRule.getBookingNotContainItems()));
        long result = database.insertWithOnConflict(DatabaseHelper.SMS_RULE_TABLE, null, smsRuleToAdd, SQLiteDatabase.CONFLICT_REPLACE);
        database.close();
        return result;
    }

    @Override
    public void unSubscribe() {
        if (mSubscription != null && !mSubscription.isUnsubscribed())
            mSubscription.unsubscribe();
    }
}
