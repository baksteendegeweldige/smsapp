package org.insacc.myapplication.Database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Created by can on 15.01.2017.
 */

public class DatabaseHelper extends SQLiteOpenHelper {

    private static DatabaseHelper mDatabaseHelper;
    // Database Info
    private static final String DATABASE_NAME = "checkFront.db";
    private static final int DATABASE_VERSION = 1;

    public static final String SMS_RULE_TABLE = "smsRules";

    public static final String SMS_RULE_CURR_HOUR = "smsRulesCurrHour";
    public static final String SMS_RULE_CURR_MINUTE = "smsRulesCurrMinute";
    public static final String SMS_RULE_IS_START_TODAY = "smsRulesIsStartToday";
    public static final String SMS_RULE_ADD_DAY = "smsRulesAddDay";
    public static final String SMS_RULE_CHECK_IN_STATUS = "smsRulesCheckInStatus";
    public static final String SMS_RULE_BOOKING_NOT_CONTAIN = "smsRulesBookingNotContain";
    public static final String SMS_RULE_BOOKING_CONTAIN = "smsRulesBookingContain";
    public static final String SMS_RULE_BOOKING_STATUS = "smsRuleBookingStatus";
    public static final String SMS_RULE_SMS_TEXT = "smsRuleSmsText";
    //public static final String SMS_RULE_IS_SMS_RECEIVED = "smsRuleIsSmsReceived";
    public static final String SMS_RULE_NAME = "smsRuleName";


    //table to save customer phone.
    public static final String CUSTOMER_PHONE_TABLE = "customerPhone";

    public static final String CUSTOMER_NAME = "customerPhoneName";
    public static final String CUSTOMER_PHONE = "customerPhone";
    public static final String CUSTOMER_INFO_DATE = "customerInfoDate";
    public static final String CUSTOMER_RESERVATION_NUMBER = "customerReservationNumber";

    //booking status table
    public static final String BOOKING_STATUS_TABLE = "bookingStatus";

    public static final String BOOKING_STATUS_NAME = "bookingStatusName";

    //booking sku items tables
    public static final String BOOKINGS_ITEMS_TABLE = "bookingItems";

    public static final String BOOKING_SKU_ITEM_CODE = "bookingSkuItem";

    private final String CREATE_BOOKING_SKU_ITEM_TABLE = "CREATE TABLE " + BOOKINGS_ITEMS_TABLE
            + "( " + BOOKING_SKU_ITEM_CODE + " TEXT PRIMARY KEY)";

    private final String CREATE_BOOKING_STATUS_TABLE = "CREATE TABLE " + BOOKING_STATUS_TABLE
            + " ( " + BOOKING_STATUS_NAME + " TEXT PRIMARY KEY)";

    private final String CREATE_SMS_RULE_TABLE = "CREATE TABLE " + SMS_RULE_TABLE
            + "( " + SMS_RULE_NAME + " TEXT PRIMARY KEY, " + SMS_RULE_CURR_HOUR + " TEXT, "
            + SMS_RULE_CURR_MINUTE + " TEXT, "
            + SMS_RULE_IS_START_TODAY + " INTEGER," + SMS_RULE_ADD_DAY + " INTEGER, "
            + SMS_RULE_CHECK_IN_STATUS + " INTEGER, " + SMS_RULE_BOOKING_CONTAIN
            + " TEXT, " + SMS_RULE_BOOKING_NOT_CONTAIN + " TEXT, " + SMS_RULE_BOOKING_STATUS
            + " TEXT, " + SMS_RULE_SMS_TEXT + " TEXT " //+ SMS_RULE_IS_SMS_RECEIVED
            //+ " INTEGER)";
            + ")";

    private final String CREATE_CUSTOMER_PHONE_TABLE = "create table " + CUSTOMER_PHONE_TABLE
            + "(" + CUSTOMER_RESERVATION_NUMBER + " REAL PRIMARY KEY," + CUSTOMER_NAME + " TEXT,"
            + CUSTOMER_PHONE + " TEXT, "+  CUSTOMER_INFO_DATE + " REAL" +  ")";

    private final String ADD_BOOKING_STATUS_DEFAULT = " INSERT INTO " + BOOKING_STATUS_TABLE
            + " ( " + BOOKING_STATUS_NAME + ") VALUES " + " (\"CONTR\"),(\"FACTU\"),(\"HERIN\"),(\"HOLD\")"
            + ",(\"INORD\"),(\"LEFAC\"),(\"LEVEB\"),(\"LEVER\"),(\"ONVOL\"),(\"OVERS\"),(\"PAID\"),"
            + "(\"PART\"),(\"PEND\")";

    private final String ADD_BOOKING_ITEM_DEFAULT = "INSERT INTO " + BOOKINGS_ITEMS_TABLE
            + "( " + BOOKING_SKU_ITEM_CODE + ") VALUES " + "(\"1opblaabaarobje\"), (\"8hornetbeerse\")," +
            "(\"8hornetgent\"),(\"8hornetherent\"),(\"antwerpen-ph\"),(\"brussel-ph\"),(\"hasselt-phoe\")" +
            ",(\"herent-phoen\"),(\"pijlenboogpakketbeerse\"),(\"pijlenboogpakkethasselt\")" +
            ",(\"pijlenboogpakketherent\"),(\"verzendingenterugsturenviabpost\"),(\"verzendingenterugsturenviabpostpijlenboog\")" +
            ",(\"verzendingenterugsturenviabpostzombie\"),(\"verzendingenterugsturenviagls\"),(\"verzendingenterugsturenviaglsnl\")" +
            ",(\"verzendingenterugsturenviaglspijlenboog\"),(\"verzendingenterugsturenviaglspijlenboognl\")" +
            ",(\"verzendingenterugsturenviaglszombie\"),(\"zombiezombiehasselt\"),(\"zombiezombieherent\")";


    private DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public static synchronized  DatabaseHelper getInstance(Context context) {
        if(mDatabaseHelper == null) {
            mDatabaseHelper = new DatabaseHelper(context);
        }

        return mDatabaseHelper;
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        Log.i("databaseCreate", "called");
        sqLiteDatabase.execSQL(CREATE_SMS_RULE_TABLE);
        sqLiteDatabase.execSQL(CREATE_CUSTOMER_PHONE_TABLE);
        sqLiteDatabase.execSQL(CREATE_BOOKING_STATUS_TABLE);
        sqLiteDatabase.execSQL(CREATE_BOOKING_SKU_ITEM_TABLE);
        sqLiteDatabase.execSQL(ADD_BOOKING_STATUS_DEFAULT);
        sqLiteDatabase.execSQL(ADD_BOOKING_ITEM_DEFAULT);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }
}
