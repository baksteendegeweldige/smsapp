package org.insacc.myapplication.Database.SentSmsDataService;

import org.insacc.myapplication.Database.BaseService;
import org.insacc.myapplication.Model.CustomerInformation;

/**
 * Created by can on 22.01.2017.
 */

public interface SentSmsDataService extends BaseService {

    void saveCustomerInformation(CustomerInformation customerInformation, SaveCustomerInfoCallback callback);

    void getCustomerInformation(String customerName, GetCustomerInfoCallback getCustomerInfoCallback);

    interface SaveCustomerInfoCallback {

        void onCustomerInfoSave();

        void onCustomerInfoSaveFail();
    }

    interface GetCustomerInfoCallback {

        void onCustomerInfoLoaded(CustomerInformation customerInformation);

        void onCustomerInfoLoadFail();
    }
}
