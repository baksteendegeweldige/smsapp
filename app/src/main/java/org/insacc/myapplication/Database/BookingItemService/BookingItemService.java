package org.insacc.myapplication.Database.BookingItemService;

import org.insacc.myapplication.Database.BaseService;

import java.util.List;

/**
 * Created by can on 27.01.2017.
 */

public interface BookingItemService extends BaseService {

    void getBookingItems(GetBookingItemsCallback callback);

    interface GetBookingItemsCallback {
        void onBookingItemsLoaded(List<String> bookingItems);

        void onBookingItemLoadFail();
    }
}
