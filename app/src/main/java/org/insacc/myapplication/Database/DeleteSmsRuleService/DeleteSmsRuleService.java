package org.insacc.myapplication.Database.DeleteSmsRuleService;

import org.insacc.myapplication.BaseService;

/**
 * Created by can on 15.02.2017.
 */

public interface DeleteSmsRuleService extends BaseService {

    void deleteSmsRule(long smsRuleId, SmsRuleDeleteCallback callback);

    interface SmsRuleDeleteCallback {

        void onSmsRuleDeleted(long deleteRuleId);

        void onSmsRuleDeleteFail();
    }
}
