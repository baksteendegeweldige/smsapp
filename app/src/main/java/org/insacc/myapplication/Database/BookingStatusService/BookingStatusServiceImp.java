package org.insacc.myapplication.Database.BookingStatusService;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import org.insacc.myapplication.Database.DatabaseHelper;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

import rx.Single;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

/**
 * Created by can on 27.01.2017.
 */

public class BookingStatusServiceImp implements BookingStatusService {
    private DatabaseHelper mDbHelper;

    private Subscription mSubscription;

    public BookingStatusServiceImp(DatabaseHelper databaseHelper) {
        this.mDbHelper = databaseHelper;
    }

    @Override
    public void getAllBookingStatuses(final GetAllStatusesCallback callback) {
        Single<List<String>> getAllStatuses = Single.fromCallable(new Callable<List<String>>() {
            @Override
            public List<String> call() throws Exception {
                return getAllBookingStatusesHelper();
            }
        })
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread());
        mSubscription = getAllStatuses.subscribe(new Action1<List<String>>() {
            @Override
            public void call(List<String> statuses) {
                if (statuses != null && statuses.size() > 0) {
                    callback.onStatusesLoaded(statuses);
                } else {
                    callback.onStatusLoadFail();
                }
            }
        });

    }

    private List<String> getAllBookingStatusesHelper() {

        SQLiteDatabase database = mDbHelper.getReadableDatabase();
        List<String> statuses = new ArrayList<>(0);

        String query = "SELECT * FROM " + DatabaseHelper.BOOKING_STATUS_TABLE;

        Cursor cursor = database.rawQuery(query, null);

        try {
            if (cursor.moveToFirst()) {
                do {

                    Log.i("bookingItem", "called");
                    statuses.add(cursor.getString(cursor.getColumnIndex(
                            DatabaseHelper.BOOKING_STATUS_NAME)));


                } while (cursor.moveToNext());
            }
        } catch (Exception e) {


        } finally {

            if (cursor != null && !cursor.isClosed()) {
                cursor.close();
            }
            database.close();
        }

        return statuses;
    }

    @Override
    public void getBookingStatus(String bookingStatus, GetBookingStatusCallback callback) {

    }

    @Override
    public void saveBookingStatus(String oldBookingStatus, String newBookingStatus, SaveStatusCallback callback) {

    }

    @Override
    public void unSubscribe() {
        if (mSubscription != null && !mSubscription.isUnsubscribed())
            mSubscription.unsubscribe();
    }
}
