package org.insacc.myapplication.DaggerModules.AddSmsRuleModule.BookingItemListModule;

import org.insacc.myapplication.AddSmsRule.BookingItemList.BookingItemListDialog;
import org.insacc.myapplication.CustomScope;
import org.insacc.myapplication.DaggerModules.AppComponent;
import org.insacc.myapplication.DaggerModules.DbHelperModule;

import dagger.Component;

/**
 * Created by can on 27.01.2017.
 */
@Component(dependencies = AppComponent.class, modules = {BookingItemListModule.class, DbHelperModule.class})
@CustomScope
public interface BookingItemListComponent {

    void inject(BookingItemListDialog dialog);
}
