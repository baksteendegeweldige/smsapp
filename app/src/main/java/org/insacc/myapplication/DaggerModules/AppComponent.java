package org.insacc.myapplication.DaggerModules;

import android.content.Context;

import org.insacc.myapplication.Database.DatabaseHelper;
import org.insacc.myapplication.Service.ApiCall;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by can on 22.12.2016.
 */
@Singleton
@Component(modules = AppModule.class)
public interface AppComponent {


    Context provideContext();

    ApiCall provideApiCall();

    DatabaseHelper provideDatabaseHelper();


}
