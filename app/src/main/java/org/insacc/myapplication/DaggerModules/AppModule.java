package org.insacc.myapplication.DaggerModules;

import android.content.Context;

import org.insacc.myapplication.Database.DatabaseHelper;
import org.insacc.myapplication.MyApp;
import org.insacc.myapplication.Service.ApiCall;
import org.insacc.myapplication.Service.ApiWrapper;
import org.insacc.myapplication.Service.Interceptors.RequestInterceptor;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by can on 22.12.2016.
 */

@Module
public class AppModule {

    private MyApp mApplication;

    public AppModule(MyApp application) {

        this.mApplication = application;
    }

    @Provides
    @Singleton
    public Context provideApplication() {
        return mApplication;
    }


    @Provides
    @Singleton
    public ApiCall provideApiCall(RequestInterceptor requestInterceptor) {
        return ApiWrapper.getInstance(requestInterceptor);
    }

    @Provides
    @Singleton
    public RequestInterceptor providesInterceptor() {
        return new RequestInterceptor();
    }


    @Provides
    @Singleton
    public DatabaseHelper providesDatabaseHelper() {
        return DatabaseHelper.getInstance(mApplication);
    }
}
