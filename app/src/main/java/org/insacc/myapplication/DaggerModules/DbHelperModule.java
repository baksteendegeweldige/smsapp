package org.insacc.myapplication.DaggerModules;

import android.content.Context;

import org.insacc.myapplication.CustomScope;
import org.insacc.myapplication.Database.AddSmsRuleService.AddSmsService;
import org.insacc.myapplication.Database.AddSmsRuleService.AddSmsServiceImp;
import org.insacc.myapplication.Database.BookingItemService.BookingItemService;
import org.insacc.myapplication.Database.BookingItemService.BookingItemServiceImp;
import org.insacc.myapplication.Database.BookingStatusService.BookingStatusService;
import org.insacc.myapplication.Database.BookingStatusService.BookingStatusServiceImp;
import org.insacc.myapplication.Database.DatabaseHelper;
import org.insacc.myapplication.Database.DeleteSmsRuleService.DeleteSmsRuleService;
import org.insacc.myapplication.Database.DeleteSmsRuleService.DeleteSmsRuleServiceImp;
import org.insacc.myapplication.Database.GetSmsRuleService.GetSmsRuleService;
import org.insacc.myapplication.Database.GetSmsRuleService.GetSmsRuleServiceImp;
import org.insacc.myapplication.Database.GetSmsRulesService.GetSmsRulesService;
import org.insacc.myapplication.Database.GetSmsRulesService.GetSmsRulesServiceImp;

import dagger.Module;
import dagger.Provides;

/**
 * Created by can on 26.01.2017.
 */
@Module
public class DbHelperModule {


    @Provides
    @CustomScope
    public AddSmsService providesAddSmsRuleService(DatabaseHelper databaseHelper) {
        return new AddSmsServiceImp(databaseHelper);
    }

    @Provides
    @CustomScope
    public BookingItemService providesBookingItemService(DatabaseHelper databaseHelper) {
        return new BookingItemServiceImp(databaseHelper);
    }

    @Provides
    @CustomScope
    public BookingStatusService providesBookingStatusService(DatabaseHelper databaseHelper) {
        return new BookingStatusServiceImp(databaseHelper);
    }

    @Provides
    @CustomScope
    public GetSmsRulesService providesRuleService(DatabaseHelper databaseHelper) {
        return new GetSmsRulesServiceImp(databaseHelper);
    }

    @Provides
    @CustomScope
    public DeleteSmsRuleService providesDeleteSmsRuleService(DatabaseHelper databaseHelper) {
        return new DeleteSmsRuleServiceImp(databaseHelper);
    }

    @Provides
    @CustomScope
    public GetSmsRuleService providesSmsRuleService(DatabaseHelper databaseHelper) {
        return new GetSmsRuleServiceImp(databaseHelper);
    }
}
