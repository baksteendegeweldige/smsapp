package org.insacc.myapplication.DaggerModules.ConfigurationModule;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import org.insacc.myapplication.Configuration.ConfigurationContract;
import org.insacc.myapplication.Configuration.ConfigurationPresenter;
import org.insacc.myapplication.CustomScope;

import dagger.Module;
import dagger.Provides;

/**
 * Created by can on 15.01.2017.
 */
@Module
public class ConfigurationModule {

    private ConfigurationContract.View mView;

    public ConfigurationModule(ConfigurationContract.View view) {
        this.mView = view;
    }

    @Provides
    @CustomScope
    public ConfigurationContract.Presenter providesPresenter() {
        return new ConfigurationPresenter(mView);
    }

    @Provides
    @CustomScope
    public SharedPreferences providesSharedPreferences(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context);
    }
}
