package org.insacc.myapplication.DaggerModules.EditSmsRuleModule;

import org.insacc.myapplication.CustomScope;
import org.insacc.myapplication.Database.AddSmsRuleService.AddSmsService;
import org.insacc.myapplication.Database.GetSmsRuleService.GetSmsRuleService;
import org.insacc.myapplication.EditSmsRule.EditSmsRuleContract;
import org.insacc.myapplication.EditSmsRule.EditSmsRulePresenter;

import dagger.Module;
import dagger.Provides;

/**
 * Created by can on 27.01.2017.
 */
@Module
public class EditSmsRuleModule {

    private EditSmsRuleContract.View mView;

    public EditSmsRuleModule(EditSmsRuleContract.View view) {
        this.mView = view;
    }

    @CustomScope
    @Provides
    public EditSmsRuleContract.Presenter providesPresenter(GetSmsRuleService getSmsRuleService,
                                                           AddSmsService addSmsService) {
        return new EditSmsRulePresenter(mView, getSmsRuleService, addSmsService);
    }

}
