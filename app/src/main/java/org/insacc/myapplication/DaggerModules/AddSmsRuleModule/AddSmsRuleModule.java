package org.insacc.myapplication.DaggerModules.AddSmsRuleModule;

import org.insacc.myapplication.AddSmsRule.AddSmsRuleContract;
import org.insacc.myapplication.AddSmsRule.AddSmsRulePresenter;
import org.insacc.myapplication.CustomScope;
import org.insacc.myapplication.Database.AddSmsRuleService.AddSmsService;

import dagger.Module;
import dagger.Provides;

/**
 * Created by can on 22.12.2016.
 */
@Module
public class AddSmsRuleModule {

    private AddSmsRuleContract.View mView;

    public AddSmsRuleModule(AddSmsRuleContract.View view) {
        this.mView = view;
    }

    @Provides
    @CustomScope
    public AddSmsRuleContract.Presenter providesPresenter(AddSmsService addSmsService) {
        return new AddSmsRulePresenter(mView, addSmsService);
    }
}
