package org.insacc.myapplication.DaggerModules.AddSmsRuleModule.BookingItemListModule;

import org.insacc.myapplication.AddSmsRule.BookingItemList.BookingItemListContract;
import org.insacc.myapplication.AddSmsRule.BookingItemList.BookingItemListDialog;
import org.insacc.myapplication.AddSmsRule.BookingItemList.BookingItemListPresenter;
import org.insacc.myapplication.CustomScope;
import org.insacc.myapplication.Database.BookingItemService.BookingItemService;

import dagger.Module;
import dagger.Provides;

/**
 * Created by can on 27.01.2017.
 */
@Module
public class BookingItemListModule {

    private BookingItemListDialog mView;

    public BookingItemListModule(BookingItemListDialog dialog) {
        this.mView = dialog;
    }

    @Provides
    @CustomScope
    public BookingItemListContract.Presenter providesPresenter(BookingItemService bookingItemService) {
        return new BookingItemListPresenter(mView, bookingItemService);
    }
}
