package org.insacc.myapplication.DaggerModules.SmsRulesModule;

import org.insacc.myapplication.CustomScope;
import org.insacc.myapplication.Database.DeleteSmsRuleService.DeleteSmsRuleService;
import org.insacc.myapplication.Database.GetSmsRulesService.GetSmsRulesService;
import org.insacc.myapplication.Service.ApiCall;
import org.insacc.myapplication.Service.GetBookingService.GetBookingService;
import org.insacc.myapplication.Service.GetBookingService.GetBookingServiceImp;
import org.insacc.myapplication.Service.GetBookingsService.GetBookingsService;
import org.insacc.myapplication.Service.GetBookingsService.GetBookingsServiceImp;
import org.insacc.myapplication.SmsRules.SmsRulesContract;
import org.insacc.myapplication.SmsRules.SmsRulesPresenter;

import dagger.Module;
import dagger.Provides;

/**
 * Created by can on 22.12.2016.
 */

@Module
public class SmsRulesModule {

    private SmsRulesContract.View mView;

    public SmsRulesModule(SmsRulesContract.View view) {
        this.mView = view;
    }

    @Provides
    @CustomScope
    public SmsRulesContract.Presenter providePresenter(GetSmsRulesService getSmsRulesService,
                                                       DeleteSmsRuleService deleteSmsRuleService) {
        return new SmsRulesPresenter(mView, getSmsRulesService, deleteSmsRuleService);


    }

    @Provides
    @CustomScope
    public GetBookingsService providesBookingsService(ApiCall apiCall) {
        return new GetBookingsServiceImp(apiCall);
    }

    @Provides
    @CustomScope
    public GetBookingService providesBookingService(ApiCall apiCall) {
        return new GetBookingServiceImp(apiCall);
    }


}
