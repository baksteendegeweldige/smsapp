package org.insacc.myapplication.DaggerModules.AddSmsRuleModule;

import org.insacc.myapplication.AddSmsRule.AddSmsRuleActivity;
import org.insacc.myapplication.CustomScope;
import org.insacc.myapplication.DaggerModules.AppComponent;
import org.insacc.myapplication.DaggerModules.DbHelperModule;
import org.insacc.myapplication.SmsRules.SmsRuleActivity;

import dagger.Component;

/**
 * Created by can on 22.12.2016.
 */
@CustomScope
@Component(dependencies = AppComponent.class, modules = {AddSmsRuleModule.class, DbHelperModule.class})
public interface AddSmsRuleComponent {

    void inject(AddSmsRuleActivity activity);
}
