package org.insacc.myapplication.DaggerModules.EditSmsRuleModule;

import org.insacc.myapplication.CustomScope;
import org.insacc.myapplication.DaggerModules.AppComponent;
import org.insacc.myapplication.DaggerModules.DbHelperModule;
import org.insacc.myapplication.EditSmsRule.EditSmsRuleActivity;

import dagger.Component;

/**
 * Created by can on 27.01.2017.
 */
@CustomScope
@Component(dependencies = AppComponent.class, modules = {EditSmsRuleModule.class, DbHelperModule.class})
public interface EditSmsRuleComponent {

    void inject(EditSmsRuleActivity activity);
}
