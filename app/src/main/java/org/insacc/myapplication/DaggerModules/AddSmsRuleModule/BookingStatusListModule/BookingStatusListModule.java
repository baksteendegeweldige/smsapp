package org.insacc.myapplication.DaggerModules.AddSmsRuleModule.BookingStatusListModule;

import org.insacc.myapplication.AddSmsRule.BookingStatusList.BookingStatusContract;
import org.insacc.myapplication.AddSmsRule.BookingStatusList.BookingStatusPresenter;
import org.insacc.myapplication.CustomScope;
import org.insacc.myapplication.Database.BookingStatusService.BookingStatusService;

import dagger.Module;
import dagger.Provides;

/**
 * Created by can on 27.01.2017.
 */
@Module
public class BookingStatusListModule {

    private BookingStatusContract.View mView;

    public BookingStatusListModule(BookingStatusContract.View view) {
        this.mView = view;
    }

    @CustomScope
    @Provides
    public BookingStatusContract.Presenter providesPresenter(BookingStatusService bookingStatusService) {
        return new BookingStatusPresenter(mView, bookingStatusService);
    }
}
