package org.insacc.myapplication.DaggerModules.AddSmsRuleModule.BookingStatusListModule;

import org.insacc.myapplication.AddSmsRule.BookingStatusList.BookingStatusDialog;
import org.insacc.myapplication.CustomScope;
import org.insacc.myapplication.DaggerModules.AddSmsRuleModule.BookingItemListModule.BookingItemListModule;
import org.insacc.myapplication.DaggerModules.AppComponent;
import org.insacc.myapplication.DaggerModules.DbHelperModule;

import dagger.Component;

/**
 * Created by can on 27.01.2017.
 */
@CustomScope
@Component(dependencies = AppComponent.class, modules = {BookingStatusListModule.class, DbHelperModule.class})
public interface BookingStatusListComponent {

    void inject(BookingStatusDialog dialog);
}
