package org.insacc.myapplication.DaggerModules.ConfigurationModule;

import org.insacc.myapplication.Configuration.ConfigurationActivity;
import org.insacc.myapplication.CustomScope;
import org.insacc.myapplication.DaggerModules.AppComponent;
import org.insacc.myapplication.DaggerModules.AppModule;

import dagger.Component;

/**
 * Created by can on 15.01.2017.
 */
@CustomScope
@Component(dependencies = AppComponent.class, modules = ConfigurationModule.class)
public interface ConfigurationComponent {

    void inject(ConfigurationActivity activity);
}
