package org.insacc.myapplication.Configuration;

/**
 * Created by can on 8.01.2017.
 */

public interface ConfigurationContract {

    interface View {

        void saveEndPoint(String endPoint);

        void saveEmailCredentials(String emailAddress, String password);

        void setCurrentEndpointUrl();

        void showFillEndpointMsg();

        void setCurrentEmailCredentials();

        void displayConfigsSavedMsg();

        void closeConfigUi();
    }

    interface Presenter {


        void callCurrentEndpoint();

        void callCurrentEmailCredentials();

        void callSaveConfiguration(String endPoint, String emailAddress, String emailPassword);
    }
}
