package org.insacc.myapplication.Configuration;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.insacc.myapplication.Config;
import org.insacc.myapplication.DaggerModules.ConfigurationModule.ConfigurationModule;
import org.insacc.myapplication.DaggerModules.ConfigurationModule.DaggerConfigurationComponent;
import org.insacc.myapplication.MyApp;
import org.insacc.myapplication.R;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by can on 8.01.2017.
 */

public class ConfigurationActivity extends AppCompatActivity implements ConfigurationContract.View {

    @Inject
    ConfigurationContract.Presenter mPresenter;
    @BindView(R.id.config_done_button)
    Button mDoneButton;
    @BindView(R.id.config_api_endpoint)
    EditText mApiEndPoint;
    @BindView(R.id.config_email_address)
    EditText mEmailAddress;
    @BindView(R.id.config_email_password)
    EditText mPassword;

    @Inject
    SharedPreferences mSharedPreferences;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.configuration_activity);

        DaggerConfigurationComponent.builder().appComponent(((MyApp) getApplicationContext()).getAppComponent())
                .configurationModule(new ConfigurationModule(this)).build().inject(this);

        ButterKnife.bind(this);

        mPresenter.callCurrentEndpoint();
        mPresenter.callCurrentEmailCredentials();

        mDoneButton.setOnClickListener(doneButtonListener());
    }

    private View.OnClickListener doneButtonListener() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mPresenter.callSaveConfiguration(mApiEndPoint.getText().toString(), mEmailAddress
                        .getText().toString(), mPassword.getText().toString());

            }
        };
    }

    @Override
    public void saveEndPoint(String endPoint) {
        mSharedPreferences.edit().putString(Config.ENDPOINT_API, endPoint).commit();
    }

    @Override
    public void saveEmailCredentials(String emailAddress, String password) {
        mSharedPreferences.edit().putString(Config.EMAIL_ADDRESS, emailAddress).commit();
        mSharedPreferences.edit().putString(Config.EMAIL_PASSWORD, password).commit();
    }

    @Override
    public void setCurrentEndpointUrl() {
        String currentEndpoint = mSharedPreferences.getString(Config.ENDPOINT_API, Config.BASE_URL);

        if (currentEndpoint != null)
            mApiEndPoint.setText(currentEndpoint);
    }


    @Override
    public void showFillEndpointMsg() {
        Toast.makeText(this, getString(R.string.fill_base_url), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void setCurrentEmailCredentials() {
        String currentEmail = mSharedPreferences.getString(Config.EMAIL_ADDRESS,
                "forjusttestpurposes@gmail.com");
        String currentPassword = mSharedPreferences.getString(Config.EMAIL_PASSWORD, "Test_?1234");

        mEmailAddress.setText(currentEmail);
        mPassword.setText(currentPassword);
    }

    @Override
    public void displayConfigsSavedMsg() {
        Toast.makeText(this, getString(R.string.changes_saved), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void closeConfigUi() {
        this.finish();
    }
}
