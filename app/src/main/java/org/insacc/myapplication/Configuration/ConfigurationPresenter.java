package org.insacc.myapplication.Configuration;

/**
 * Created by can on 8.01.2017.
 */

public class ConfigurationPresenter implements ConfigurationContract.Presenter {

    private ConfigurationContract.View mView;

    public ConfigurationPresenter(ConfigurationContract.View view) {
        this.mView = view;

    }

    @Override
    public void callCurrentEndpoint() {
        mView.setCurrentEndpointUrl();
    }

    @Override
    public void callCurrentEmailCredentials() {
        mView.setCurrentEmailCredentials();
    }

    @Override
    public void callSaveConfiguration(String endPoint, String emailAddress, String emailPassword) {
        if (endPoint != null && endPoint.length() > 0 && emailAddress != null && emailAddress.length() > 0
                && emailPassword != null && emailPassword.length() > 0) {

            mView.saveEndPoint(endPoint);
            mView.saveEmailCredentials(emailAddress, emailPassword);
            mView.displayConfigsSavedMsg();
            mView.closeConfigUi();

        } else {
            mView.showFillEndpointMsg();
        }

    }
}
