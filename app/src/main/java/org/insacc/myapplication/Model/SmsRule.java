package org.insacc.myapplication.Model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

/**
 * Created by can on 27.12.2016.
 */

public class SmsRule implements Parcelable {

    private String smsToSend;

    private int checkInOutStatus;

    private List<BookingSkuItem> bookingContainItems;

    private List<BookingSkuItem> bookingNotContainItems;

    private String ruleName;

    //private boolean isReceivedSms;

    private int additionalDayCount;

    private boolean isStartDayToday;

    private String mCurrentHour;

    private String mCurrentMinute;

    private String mBookingStatus;

    private long id;

    public SmsRule() {

    }

    protected SmsRule(Parcel in) {
        smsToSend = in.readString();
        checkInOutStatus = in.readInt();
        bookingContainItems = in.createTypedArrayList(BookingSkuItem.CREATOR);
        bookingNotContainItems = in.createTypedArrayList(BookingSkuItem.CREATOR);
        ruleName = in.readString();
        additionalDayCount = in.readInt();
        isStartDayToday = in.readByte() != 0;
        mCurrentHour = in.readString();
        mCurrentMinute = in.readString();
        mBookingStatus = in.readString();
        id = in.readLong();
    }

    public static final Creator<SmsRule> CREATOR = new Creator<SmsRule>() {
        @Override
        public SmsRule createFromParcel(Parcel in) {
            return new SmsRule(in);
        }

        @Override
        public SmsRule[] newArray(int size) {
            return new SmsRule[size];
        }
    };

    public String getmCurrentHour() {
        return mCurrentHour;
    }

    public void setmCurrentHour(String mCurrentHour) {
        this.mCurrentHour = mCurrentHour;
    }

    public String getmCurrentMinute() {
        return mCurrentMinute;
    }

    public void setmCurrentMinute(String mCurrentMinute) {
        this.mCurrentMinute = mCurrentMinute;
    }

    public String getmBookingStatus() {
        return mBookingStatus;
    }

    public void setmBookingStatus(String mBookingStatus) {
        this.mBookingStatus = mBookingStatus;
    }


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }


    public List<BookingSkuItem> getBookingContainItems() {
        return bookingContainItems;
    }

    public void setBookingContainItems(List<BookingSkuItem> bookingContainItems) {
        this.bookingContainItems = bookingContainItems;
    }

    public List<BookingSkuItem> getBookingNotContainItems() {
        return bookingNotContainItems;
    }

    public void setBookingNotContainItems(List<BookingSkuItem> bookingNotContainItems) {
        this.bookingNotContainItems = bookingNotContainItems;
    }

    public String getRuleName() {
        return ruleName;
    }

    public void setRuleName(String ruleName) {
        this.ruleName = ruleName;
    }

    public String getSmsToSend() {
        return smsToSend;
    }

    public void setSmsToSend(String smsToSend) {
        this.smsToSend = smsToSend;
    }

    public int getCheckInOutStatus() {
        return checkInOutStatus;
    }

    public void setCheckInOutStatus(int checkInOutStatus) {
        this.checkInOutStatus = checkInOutStatus;
    }

    public boolean isStartDayToday() {
        return isStartDayToday;
    }

    public void setStartDayToday(boolean startDayToday) {
        isStartDayToday = startDayToday;
    }

    public int getAdditionalDayCount() {
        return additionalDayCount;
    }

    public void setAdditionalDayCount(int additionalDayCount) {
        this.additionalDayCount = additionalDayCount;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(smsToSend);
        dest.writeInt(checkInOutStatus);
        dest.writeTypedList(bookingContainItems);
        dest.writeTypedList(bookingNotContainItems);
        dest.writeString(ruleName);
        dest.writeInt(additionalDayCount);
        dest.writeByte((byte) (isStartDayToday ? 1 : 0));
        dest.writeString(mCurrentHour);
        dest.writeString(mCurrentMinute);
        dest.writeString(mBookingStatus);
        dest.writeLong(id);
    }
/*
    public boolean isReceivedSms() {
        return isReceivedSms;
    }

    public void setReceivedSms(boolean receivedSms) {
        isReceivedSms = receivedSms;
    }
*/
}
