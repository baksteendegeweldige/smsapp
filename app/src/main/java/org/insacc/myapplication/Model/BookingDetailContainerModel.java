package org.insacc.myapplication.Model;

import java.util.HashMap;

/**
 * Created by can on 3.02.2017.
 */

public class BookingDetailContainerModel {

    public HashMap<String, BookingDetail> getBooking() {
        return booking;
    }

    public void setBooking(HashMap<String, BookingDetail> booking) {
        this.booking = booking;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    private HashMap<String, BookingDetail> booking;

    private String name;


}
