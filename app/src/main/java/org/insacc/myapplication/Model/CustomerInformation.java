package org.insacc.myapplication.Model;

/**
 * Created by can on 22.01.2017.
 */

public class CustomerInformation {

    private String customerName;
    private String phoneNumber;
    private long reservationID;

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public long getReservationID() {
        return reservationID;
    }

    public void setReservationID(long reservationID) {
        this.reservationID = reservationID;
    }


}
