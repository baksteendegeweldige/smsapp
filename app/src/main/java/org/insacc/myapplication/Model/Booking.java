package org.insacc.myapplication.Model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by can on 19.01.2017.
 */

public class Booking {

    @SerializedName("booking_id")
    private int bookingId;
    private String code;
    @SerializedName("status_id")
    private String statusId;
    @SerializedName("status_name")
    private String statusName;

    //TODO add timestamp createdDate

    private float total;
    @SerializedName("tax_total")
    private float taxTotal;
    @SerializedName("paid_total")
    private float paidTotal;

    @SerializedName("customer_name")
    private String customerName;
    @SerializedName("customer_email")
    private String customerEmail;

    private String summary;
    @SerializedName("date_desc")
    private String dateDesc;

    private String token;

    public int getBookingId() {
        return bookingId;
    }

    public void setBookingId(int bookingId) {
        this.bookingId = bookingId;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getStatusId() {
        return statusId;
    }

    public void setStatusId(String statusId) {
        this.statusId = statusId;
    }

    public String getStatusName() {
        return statusName;
    }

    public void setStatusName(String statusName) {
        this.statusName = statusName;
    }

    public float getTotal() {
        return total;
    }

    public void setTotal(float total) {
        this.total = total;
    }

    public float getTaxTotal() {
        return taxTotal;
    }

    public void setTaxTotal(float taxTotal) {
        this.taxTotal = taxTotal;
    }

    public float getPaidTotal() {
        return paidTotal;
    }

    public void setPaidTotal(float paidTotal) {
        this.paidTotal = paidTotal;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getCustomerEmail() {
        return customerEmail;
    }

    public void setCustomerEmail(String customerEmail) {
        this.customerEmail = customerEmail;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public String getDateDesc() {
        return dateDesc;
    }

    public void setDateDesc(String dateDesc) {
        this.dateDesc = dateDesc;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }


}
