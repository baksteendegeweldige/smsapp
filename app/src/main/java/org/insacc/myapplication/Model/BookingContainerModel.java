package org.insacc.myapplication.Model;

import com.google.gson.annotations.SerializedName;

import org.insacc.myapplication.Model.Booking;

import java.util.HashMap;

/**
 * Created by can on 3.02.2017.
 */

public class BookingContainerModel {

    private String name;

    public HashMap<String, Booking> getBookingIndex() {
        return bookingIndex;
    }

    public void setBookingIndex(HashMap<String, Booking> bookingIndex) {
        this.bookingIndex = bookingIndex;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @SerializedName("booking/index")
    private HashMap<String, Booking> bookingIndex;
}
