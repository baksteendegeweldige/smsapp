package org.insacc.myapplication.Model;

import java.util.HashMap;
import java.util.List;

/**
 * Created by can on 1.02.2017.
 */

public class BookingDetail {


    private String id;

    private long booking_id;

    private String status_id;
    private int checkout;
    private int checkin;
    private String customer_name;
    private String customer_address;
    private String customer_city;
    private String customer_region;
    private String customer_postal_zip;
    private String customer_country;

    private String customer_email;
    private String customer_phone;
    private String customer_id;
    private String token;


   private HashMap<String, Item> items;

    public HashMap<String, Item> getItems() {
        return items;
    }

    public void setItems(HashMap<String, Item> items) {
        this.items = items;
    }


    public String getCustomer_email() {
        return customer_email;
    }

    public void setCustomer_email(String customer_email) {
        this.customer_email = customer_email;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public long getBooking_id() {
        return booking_id;
    }

    public void setBooking_id(long booking_id) {
        this.booking_id = booking_id;
    }

    public String getStatus_id() {
        return status_id;
    }

    public void setStatus_id(String status_id) {
        this.status_id = status_id;
    }

    public int getCheckout() {
        return checkout;
    }

    public void setCheckout(int checkout) {
        this.checkout = checkout;
    }

    public int getCheckin() {
        return checkin;
    }

    public void setCheckin(int checkin) {
        this.checkin = checkin;
    }

    public String getCustomer_name() {
        return customer_name;
    }

    public void setCustomer_name(String customer_name) {
        this.customer_name = customer_name;
    }

    public String getCustomer_address() {
        return customer_address;
    }

    public void setCustomer_address(String customer_address) {
        this.customer_address = customer_address;
    }

    public String getCustomer_city() {
        return customer_city;
    }

    public void setCustomer_city(String customer_city) {
        this.customer_city = customer_city;
    }

    public String getCustomer_region() {
        return customer_region;
    }

    public void setCustomer_region(String customer_region) {
        this.customer_region = customer_region;
    }

    public String getCustomer_postal_zip() {
        return customer_postal_zip;
    }

    public void setCustomer_postal_zip(String customer_postal_zip) {
        this.customer_postal_zip = customer_postal_zip;
    }

    public String getCustomer_country() {
        return customer_country;
    }

    public void setCustomer_country(String customer_country) {
        this.customer_country = customer_country;
    }

    public String getCustomer_phone() {
        return customer_phone;
    }

    public void setCustomer_phone(String customer_phone) {
        this.customer_phone = customer_phone;
    }

    public String getCustomer_id() {
        return customer_id;
    }

    public void setCustomer_id(String customer_id) {
        this.customer_id = customer_id;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

}
