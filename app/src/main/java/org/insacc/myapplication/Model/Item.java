package org.insacc.myapplication.Model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by can on 1.02.2017.
 */

public class Item {
    @SerializedName("status_id")
    private String mStatusId;



    private int id;
    private String sku;
    private String unit;
    private String name;
    @SerializedName("qty")
    private int quantity;

    @SerializedName("tax_total")
    private String mTaxTotal;
    @SerializedName("tax_inc_total")
    private String mTaxIncludedTotal;

    @SerializedName("item_total")
    private String mItemTotal;

    @SerializedName("sub_total")
    private String mSubTotal;

    private String total;

    private String summary;

    public String getmTaxIncludedTotal() {
        return mTaxIncludedTotal;
    }

    public void setmTaxIncludedTotal(String mTaxIncludedTotal) {
        this.mTaxIncludedTotal = mTaxIncludedTotal;
    }

    public String getmStatusId() {
        return mStatusId;
    }

    public void setmStatusId(String mStatusId) {
        this.mStatusId = mStatusId;
    }

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getmTaxTotal() {
        return mTaxTotal;
    }

    public void setmTaxTotal(String mTaxTotal) {
        this.mTaxTotal = mTaxTotal;
    }

    public String getmItemTotal() {
        return mItemTotal;
    }

    public void setmItemTotal(String mItemTotal) {
        this.mItemTotal = mItemTotal;
    }

    public String getmSubTotal() {
        return mSubTotal;
    }

    public void setmSubTotal(String mSubTotal) {
        this.mSubTotal = mSubTotal;
    }
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return name + ", " + mSubTotal + ", " + mStatusId;
    }
}
