package org.insacc.myapplication.Model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

/**
 * Created by can on 27.01.2017.
 */

public class BookingSkuItem implements Parcelable {



    private List<String> itemNames;

    private boolean isSelected;

    protected BookingSkuItem(Parcel in) {
        itemNames = in.createStringArrayList();
        isSelected = in.readByte() != 0;
    }

    public static final Creator<BookingSkuItem> CREATOR = new Creator<BookingSkuItem>() {
        @Override
        public BookingSkuItem createFromParcel(Parcel in) {
            return new BookingSkuItem(in);
        }

        @Override
        public BookingSkuItem[] newArray(int size) {
            return new BookingSkuItem[size];
        }
    };

    public List<String> getItemName() {
        return itemNames;
    }

    public void setItemName(List<String> itemName) {
        this.itemNames = itemName;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public BookingSkuItem(List<String> itemName) {
        this.itemNames = itemName;
    }

    @Override
    public String toString() {

        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("(");
        for (int i = 0; i < itemNames.size(); i++) {
            stringBuilder.append(itemNames.get(i));
            if (i != itemNames.size() - 1)
                stringBuilder.append(" And ");
        }
        stringBuilder.append(" )");

        return stringBuilder.toString();
    }

    public String convertToDbItem() {
        StringBuilder stringBuilder = new StringBuilder();

        for (int i = 0; i < itemNames.size(); i++) {
            stringBuilder.append(itemNames.get(i));
            if (i != itemNames.size() - 1)
                stringBuilder.append(",");
        }

        return stringBuilder.toString();
    }

    public void setItemsFromDb(String items) {


    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeStringList(itemNames);
        dest.writeByte((byte) (isSelected ? 1 : 0));
    }
}
