package org.insacc.myapplication.Model;

/**
 * Created by can on 31.01.2017.
 */

public class Customer {

    private String mCustomerPhone;

    private String mCustomerEmail;

    private String mCustomerName;

    private String mCustomerId;

    public String getmCustomerPhone() {
        return mCustomerPhone;
    }

    public void setmCustomerPhone(String mCustomerPhone) {
        this.mCustomerPhone = mCustomerPhone;
    }

    public String getmCustomerEmail() {
        return mCustomerEmail;
    }

    public void setmCustomerEmail(String mCustomerEmail) {
        this.mCustomerEmail = mCustomerEmail;
    }

    public String getmCustomerName() {
        return mCustomerName;
    }

    public void setmCustomerName(String mCustomerName) {
        this.mCustomerName = mCustomerName;
    }

    public String getmCustomerId() {
        return mCustomerId;
    }

    public void setmCustomerId(String mCustomerId) {
        this.mCustomerId = mCustomerId;
    }


}
