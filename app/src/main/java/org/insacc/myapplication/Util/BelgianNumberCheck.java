package org.insacc.myapplication.Util;

import android.util.Log;

/**
 * Created by can on 22.12.2016.
 */

public class BelgianNumberCheck {

    public BelgianNumberCheck() {

    }

    private static String BELGIAN_PHONE_NUMBER = "^((\\+|00)32\\s?|0)(\\d\\s?\\d{3}|\\d{2}\\s?\\d{2})(\\s?\\d{2}){2}$";

    private static String BELGIAN_NUMBER_CHECK = "^((\\+|00)32\\s?|0)4(60|[789]\\d)(\\s?\\d{2}){3}$";

    public boolean isBelgianNumber(String phoneNumber) {


        // phoneNumber.matches(BELGIAN_PHONE_NUMBER);
        boolean result1 = phoneNumber.matches(BELGIAN_PHONE_NUMBER);
        //Log.i("phoneBelgianNumber", phoneNumber);
        //  Log.i("belgianNumber", String.valueOf(result));
        Log.i("belgianNumber1", String.valueOf(result1));

        return phoneNumber.matches(BELGIAN_NUMBER_CHECK) || phoneNumber.matches(BELGIAN_PHONE_NUMBER);
    }
}
