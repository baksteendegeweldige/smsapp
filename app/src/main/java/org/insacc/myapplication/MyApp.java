package org.insacc.myapplication;

import android.app.Application;
import android.content.Intent;

import org.insacc.myapplication.DaggerModules.AppComponent;
import org.insacc.myapplication.DaggerModules.AppModule;
import org.insacc.myapplication.DaggerModules.DaggerAppComponent;
import org.insacc.myapplication.Service.RebootRecoverService.RebootRecoverService;

/**
 * Created by can on 27.12.2016.
 */

public class MyApp extends Application {

    private AppComponent mAppComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        this.mAppComponent =  DaggerAppComponent.builder()
               .appModule(new AppModule(this))
                .build();

        startService(new Intent(this, RebootRecoverService.class));
    }



    public AppComponent getAppComponent() {
        return mAppComponent;
    }
}
