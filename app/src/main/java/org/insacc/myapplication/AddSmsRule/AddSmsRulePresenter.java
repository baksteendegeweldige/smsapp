package org.insacc.myapplication.AddSmsRule;

import android.util.Log;

import org.insacc.myapplication.Database.AddSmsRuleService.AddSmsService;
import org.insacc.myapplication.Model.SmsRule;

/**
 * Created by can on 22.12.2016.
 */

public class AddSmsRulePresenter implements AddSmsRuleContract.Presenter, AddSmsService.AddSmsRuleCallback {

    private AddSmsRuleContract.View mView;

    private AddSmsService mAddSmsService;

    private SmsRule mSmsRule;

    private boolean isEditSmsRule;

    public AddSmsRulePresenter(AddSmsRuleContract.View view, AddSmsService addSmsService) {
        this.mView = view;
        this.mAddSmsService = addSmsService;
    }


    @Override
    public void saveSmsRule(SmsRule smsRuleToAdd, boolean isEditSmsRule) {

        if (smsRuleToAdd == null) {
            mView.fillAllFieldsMsg();
        } else {
            this.isEditSmsRule = isEditSmsRule;
            this.mSmsRule = smsRuleToAdd;
            mAddSmsService.addSmsRule(smsRuleToAdd, this);
        }

    }

    @Override
    public void callSmsRulesUi() {
        mView.openSmsRulesUi();
    }

    @Override
    public void callAddNewContainItem() {
        mView.openNewContainItemUi();
    }

    @Override
    public void callAddNewNotContainItem() {
        mView.openNewNotContainItemUi();
    }

    @Override
    public void callBookingStatusListUi() {
        mView.openBookingStatusListUi();
    }

    @Override
    public void onSmsRuleAdded(long smsRuleId) {
        mView.showSmsRuleSavedMsg();
        mSmsRule.setId(smsRuleId);
        if (isEditSmsRule) {
            mView.editAlarmForRule(mSmsRule);
        } else {
            mView.createAlarmForRule(mSmsRule);
        }

        mSmsRule = null;
        mView.openSmsRulesUi();
    }

    @Override
    public void onSmsRuleAddFailed() {
        mView.showSmsRuleSaveFail();

    }
}
