package org.insacc.myapplication.AddSmsRule.BookingStatusList;

import java.util.List;

/**
 * Created by can on 27.01.2017.
 */

public interface BookingStatusContract {

    interface View {

        void bookingStatusClicked(String statusName);

        void sendSelectedStatus(String statusName);

        void updateStatusList(List<String> statusList);

        void displayStatusesLoadFailMsg();

        void closeDialog();
    }

    interface Presenter {
        void callSendSelectedStatus(String statusName);

        void populateStatusList();

        void callCloseDialog();
    }
}
