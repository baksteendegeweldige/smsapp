package org.insacc.myapplication.AddSmsRule.BookingStatusList;

import org.insacc.myapplication.Database.BookingStatusService.BookingStatusService;

import java.util.List;

/**
 * Created by can on 27.01.2017.
 */

public class BookingStatusPresenter implements BookingStatusContract.Presenter, BookingStatusService.GetAllStatusesCallback {

    private BookingStatusContract.View mView;

    private BookingStatusService mBookingStatusService;

    public BookingStatusPresenter(BookingStatusContract.View view, BookingStatusService bookingStatusService) {
        this.mView = view;
        this.mBookingStatusService = bookingStatusService;
    }

    @Override
    public void callSendSelectedStatus(String statusName) {
        mView.sendSelectedStatus(statusName);
    }

    @Override
    public void populateStatusList() {
        mBookingStatusService.getAllBookingStatuses(this);
    }

    @Override
    public void callCloseDialog() {
        mView.closeDialog();
    }

    @Override
    public void onStatusesLoaded(List<String> statuses) {
        mView.updateStatusList(statuses);
    }

    @Override
    public void onStatusLoadFail() {
        mView.displayStatusesLoadFailMsg();
    }
}
