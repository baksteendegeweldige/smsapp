package org.insacc.myapplication.AddSmsRule.BookingItemList;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Toast;

import org.insacc.myapplication.Config;
import org.insacc.myapplication.DaggerModules.AddSmsRuleModule.BookingItemListModule.BookingItemListModule;
import org.insacc.myapplication.DaggerModules.AddSmsRuleModule.BookingItemListModule.DaggerBookingItemListComponent;
import org.insacc.myapplication.DaggerModules.DbHelperModule;
import org.insacc.myapplication.Model.BookingSkuItem;
import org.insacc.myapplication.MyApp;
import org.insacc.myapplication.R;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by can on 27.01.2017.
 */

public class BookingItemListDialog extends DialogFragment implements BookingItemListContract.View {


    @Inject
    BookingItemListContract.Presenter mPresenter;

    private BookingItemListAdapter mListAdapter;
    private LinearLayoutManager mLayoutManager;
    private OnItemsPickedListener mCallback;

    @BindView(R.id.booking_sku_list_cancel)
    ImageButton mCancelButton;

    @BindView(R.id.add_booking_sku_button)
    Button mDoneButton;


    @BindView(R.id.booking_sku_item_list)
    RecyclerView mBookingItemList;

    public static BookingItemListDialog newInstance(boolean isContainedItem) {
        Bundle bundle = new Bundle();
        bundle.putBoolean(Config.IS_CONTAINED_ITEM_DIALOG, isContainedItem);
        BookingItemListDialog bookingItemListDialog = new BookingItemListDialog();
        bookingItemListDialog.setArguments(bundle);
        return bookingItemListDialog;
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.booking_sku_list, container, false);

        DaggerBookingItemListComponent.builder().bookingItemListModule(new BookingItemListModule(this))
                .appComponent(((MyApp) getActivity().getApplicationContext()).getAppComponent())
                .dbHelperModule(new DbHelperModule())
                .build().inject(this);
        ButterKnife.bind(this, root);


        mLayoutManager = new LinearLayoutManager(getActivity());
        mLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mBookingItemList.setLayoutManager(mLayoutManager);
        mBookingItemList.setAdapter(mListAdapter);

        mCancelButton.setOnClickListener(providesButtonListener());
        mDoneButton.setOnClickListener(providesButtonListener());

        mPresenter.getBookingItems();

        return root;
    }

    private View.OnClickListener providesButtonListener() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                switch (view.getId()) {

                    case R.id.booking_sku_list_cancel:
                        mPresenter.callCloseDialog();
                        break;

                    case R.id.add_booking_sku_button:
                        mPresenter.callReturnSelected();
                        break;

                }
            }
        };
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mListAdapter = new BookingItemListAdapter(new ArrayList<String>(0));
        setStyle(DialogFragment.STYLE_NO_TITLE, R.style.FullScreenDialog);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        try {
            mCallback = (OnItemsPickedListener) context;
        } catch (ClassCastException e) {
        }
    }

    @Override
    public void returnSelectedItems() {
        if (mListAdapter.getSelectedItems().getItemName().size() > 0) {
            Bundle bundle = getArguments();
            if (bundle.getBoolean(Config.IS_CONTAINED_ITEM_DIALOG)) {
                mCallback.onItemsPicked(mListAdapter.getSelectedItems());

            } else {
                mCallback.onNotContainedItemsPicked(mListAdapter.getSelectedItems());
            }

            this.dismiss();
        } else {
            Toast.makeText(getActivity(), getString(R.string.select_one_sku), Toast.LENGTH_SHORT).show();
        }


    }

    @Override
    public void setBookingItemList(List<String> bookingItemList) {
        mListAdapter.updateList(bookingItemList);
    }

    @Override
    public void displayItemLoadError() {
        Toast.makeText(getContext(), getString(R.string.booking_sku_item_fetch_err),
                Toast.LENGTH_SHORT).show();
    }

    @Override
    public void closeDialog() {
        this.dismiss();
    }


    public interface OnItemsPickedListener {
        void onItemsPicked(BookingSkuItem selectedItems);

        void onNotContainedItemsPicked(BookingSkuItem selectedNotContainItems);
    }

}
