package org.insacc.myapplication.AddSmsRule;

import org.insacc.myapplication.Model.SmsRule;

/**
 * Created by can on 22.12.2016.
 */

public interface AddSmsRuleContract {

    interface View {
        void showSmsRuleSavedMsg();

        void showSmsRuleSaveFail();

        void openSmsRulesUi();

        void openNewContainItemUi();

        void openNewNotContainItemUi();

        void openBookingStatusListUi();

        void createAlarmForRule(SmsRule smsRule);

        void fillAllFieldsMsg();

        void removeContainItem(int position);

        void removeNotContainItem(int position);

        void editAlarmForRule(SmsRule smsRule);

    }

    interface Presenter {
        void saveSmsRule(SmsRule smsRuleToAdd, boolean isEditSmsRule);

        void callSmsRulesUi();

        void callAddNewContainItem();

        void callAddNewNotContainItem();

        void callBookingStatusListUi();

    }
}
