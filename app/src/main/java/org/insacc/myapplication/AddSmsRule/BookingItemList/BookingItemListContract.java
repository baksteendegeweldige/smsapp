package org.insacc.myapplication.AddSmsRule.BookingItemList;

import java.util.List;

/**
 * Created by can on 27.01.2017.
 */

public interface BookingItemListContract {

    interface View {
        void returnSelectedItems();

        void setBookingItemList(List<String> bookingItemList);

        void displayItemLoadError();

        void closeDialog();

    }

    interface Presenter {
        void callReturnSelected();

        void getBookingItems();

        void callCloseDialog();
    }
}
