package org.insacc.myapplication.AddSmsRule.BookingItemList;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;

import org.insacc.myapplication.Model.Booking;
import org.insacc.myapplication.Model.BookingSkuItem;
import org.insacc.myapplication.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by can on 27.01.2017.
 */

public class BookingItemListAdapter extends RecyclerView.Adapter<BookingItemListAdapter.BookingItemViewHolder> {

    private List<String> bookingItems;

    private List<String> selectedItems;

    private List<Boolean> mSelectedIndexes;

    public BookingItemListAdapter(List<String> bookingItems) {
        this.bookingItems = bookingItems;
        this.selectedItems = new ArrayList<>(0);
        this.mSelectedIndexes = new ArrayList<>(40);
        fillSelectedIndexesList(40);

    }

    private void fillSelectedIndexesList(int size) {
        for (int i = 0; i < size; i++) {
            mSelectedIndexes.add(false);
        }
    }

    public void updateList(List<String> bookingItemToBeAdded) {
        this.bookingItems.addAll(bookingItemToBeAdded);

        notifyDataSetChanged();
    }

    public BookingSkuItem getSelectedItems() {

        BookingSkuItem bookingSkuItems = new BookingSkuItem(selectedItems);

        return bookingSkuItems;
    }

    @Override
    public BookingItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View root = LayoutInflater.from(parent.getContext()).inflate(R.layout.booking_sku_list_item, null);
        return new BookingItemViewHolder(root);
    }

    @Override
    public void onBindViewHolder(BookingItemViewHolder holder, int position) {
        holder.mBookingItemCheckbox.setOnCheckedChangeListener(null);
        if (mSelectedIndexes.size() > position && mSelectedIndexes.get(position)) {
            holder.mBookingItemCheckbox.setChecked(true);

        } else {
            holder.mBookingItemCheckbox.setChecked(false);
        }

        holder.mBookingItemCheckbox.setText(bookingItems.get(position));
        holder.mBookingItemCheckbox.setOnCheckedChangeListener(providesCheckBoxListener(
                bookingItems.get(position), position));


    }

    private CompoundButton.OnCheckedChangeListener providesCheckBoxListener(final String itemName, final int position) {
        return new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if (isChecked) {
                    selectedItems.add(itemName);
                    mSelectedIndexes.set(position, true);
                } else {
                    selectedItems.remove(itemName);
                    mSelectedIndexes.set(position, false);
                }
            }
        };
    }

    @Override
    public int getItemCount() {
        return bookingItems.size();
    }

    public static class BookingItemViewHolder extends RecyclerView.ViewHolder {

        private CheckBox mBookingItemCheckbox;

        public BookingItemViewHolder(View itemView) {
            super(itemView);
            mBookingItemCheckbox = (CheckBox) itemView.findViewById(R.id.booking_sku_item_checkbox);
        }


    }
}
