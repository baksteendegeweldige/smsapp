package org.insacc.myapplication.AddSmsRule.BookingStatusList;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.RadioButton;

import org.insacc.myapplication.R;

import java.util.List;

/**
 * Created by can on 27.01.2017.
 */

public class BookingStatusListAdapter extends RecyclerView.Adapter<BookingStatusListAdapter
        .BookingStatusViewHolder> {

    private List<String> mBookingStatusList;
    private BookingStatusContract.View mView;

    public BookingStatusListAdapter(List<String> bookingStatusList, BookingStatusContract.View view) {
        this.mBookingStatusList = bookingStatusList;
        this.mView = view;
    }

    public void updateStatusList(List<String> mBookingStatusList) {
        this.mBookingStatusList = mBookingStatusList;
        notifyDataSetChanged();
    }

    @Override
    public BookingStatusViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View root = LayoutInflater.from(parent.getContext()).inflate(R.layout.booking_status_list_item,
                parent, false);
        return new BookingStatusViewHolder(root);
    }

    @Override
    public void onBindViewHolder(BookingStatusViewHolder holder, final int position) {
        holder.mStatusButton.setText(mBookingStatusList.get(position));

        holder.mStatusButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mView.bookingStatusClicked(mBookingStatusList.get(position));
            }
        });
    }

    @Override
    public int getItemCount() {
        return mBookingStatusList.size();
    }

    public static class BookingStatusViewHolder extends RecyclerView.ViewHolder {

        private Button mStatusButton;

        public BookingStatusViewHolder(View itemView) {
            super(itemView);
            mStatusButton = (Button) itemView.findViewById(R.id.booking_status_sku_item_button);
        }
    }
}
