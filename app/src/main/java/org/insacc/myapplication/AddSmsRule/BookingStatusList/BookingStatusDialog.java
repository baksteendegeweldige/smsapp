package org.insacc.myapplication.AddSmsRule.BookingStatusList;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.Toast;

import org.insacc.myapplication.AddSmsRule.BookingItemList.BookingItemListDialog;
import org.insacc.myapplication.DaggerModules.AddSmsRuleModule.BookingStatusListModule.BookingStatusListModule;
import org.insacc.myapplication.DaggerModules.AddSmsRuleModule.BookingStatusListModule.DaggerBookingStatusListComponent;
import org.insacc.myapplication.DaggerModules.DbHelperModule;
import org.insacc.myapplication.MyApp;
import org.insacc.myapplication.R;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by can on 27.01.2017.
 */

public class BookingStatusDialog extends DialogFragment implements BookingStatusContract.View {

    @Inject
    BookingStatusContract.Presenter mPresenter;

    @BindView(R.id.booking_status_recycler_view)
    RecyclerView mStatusList;

    @BindView(R.id.booking_status_list_cancel)
    ImageButton mCancelButton;


    private SelectedStatusCallback mCallback;
    private BookingStatusListAdapter mListAdapter;
    private LinearLayoutManager mLayoutManager;

    public static BookingStatusDialog newInstance() {
        return new BookingStatusDialog();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.booking_status_list, container, false);

        ButterKnife.bind(this, root);
        DaggerBookingStatusListComponent.builder().bookingStatusListModule(new BookingStatusListModule(this))
                .appComponent(((MyApp) getActivity().getApplicationContext()).getAppComponent())
                .dbHelperModule(new DbHelperModule()).build().inject(this);
        mLayoutManager = new LinearLayoutManager(getActivity());
        mLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mStatusList.setLayoutManager(mLayoutManager);
        mStatusList.setAdapter(mListAdapter);

        mCancelButton.setOnClickListener(providesButtonListener());
        mPresenter.populateStatusList();

        return root;
    }

    private View.OnClickListener providesButtonListener() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                switch (view.getId()) {
                    case R.id.booking_status_list_cancel:
                        mPresenter.callCloseDialog();
                        break;
                }


            }
        };
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mListAdapter = new BookingStatusListAdapter(new ArrayList<String>(0), this);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        try {
            mCallback = (SelectedStatusCallback) context;
        } catch (ClassCastException e) {
        }
    }

    @Override
    public void bookingStatusClicked(String statusName) {
        mPresenter.callSendSelectedStatus(statusName);
    }

    @Override
    public void sendSelectedStatus(String statusName) {
        mCallback.onStatusSelected(statusName);
        this.dismiss();
    }

    @Override
    public void updateStatusList(List<String> statusList) {
        mListAdapter.updateStatusList(statusList);
    }

    @Override
    public void displayStatusesLoadFailMsg() {
        Toast.makeText(getContext(), getString(R.string.booking_status_item_fetch_err),
                Toast.LENGTH_SHORT).show();
    }

    @Override
    public void closeDialog() {
        this.dismiss();
    }

    public interface SelectedStatusCallback {
        void onStatusSelected(String name);
    }
}
