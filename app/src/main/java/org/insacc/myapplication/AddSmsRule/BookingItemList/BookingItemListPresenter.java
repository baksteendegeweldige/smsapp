package org.insacc.myapplication.AddSmsRule.BookingItemList;

import org.insacc.myapplication.Database.BookingItemService.BookingItemService;

import java.util.List;

/**
 * Created by can on 27.01.2017.
 */

public class BookingItemListPresenter implements BookingItemListContract.Presenter,
        BookingItemService.GetBookingItemsCallback {

    private BookingItemListContract.View mView;

    private BookingItemService bookingItemService;

    public BookingItemListPresenter(BookingItemListContract.View view, BookingItemService bookingItemService) {
        this.mView = view;
        this.bookingItemService = bookingItemService;
    }

    @Override
    public void callReturnSelected() {
        mView.returnSelectedItems();
    }

    @Override
    public void getBookingItems() {
        bookingItemService.getBookingItems(this);
    }

    @Override
    public void callCloseDialog() {
        mView.closeDialog();
    }

    @Override
    public void onBookingItemsLoaded(List<String> bookingItems) {

        mView.setBookingItemList(bookingItems);
    }

    @Override
    public void onBookingItemLoadFail() {
        mView.displayItemLoadError();
    }
}
