package org.insacc.myapplication.AddSmsRule;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.Image;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.NumberPicker;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TimePicker;
import android.widget.Toast;

import org.insacc.myapplication.AddSmsRule.BookingItemList.BookingItemListDialog;
import org.insacc.myapplication.AddSmsRule.BookingStatusList.BookingStatusDialog;
import org.insacc.myapplication.AddSmsRule.InAcitivityItemList.InActivityItemListAdapter;
import org.insacc.myapplication.Config;
import org.insacc.myapplication.DaggerModules.AddSmsRuleModule.AddSmsRuleModule;
import org.insacc.myapplication.DaggerModules.AddSmsRuleModule.DaggerAddSmsRuleComponent;
import org.insacc.myapplication.DaggerModules.DbHelperModule;
import org.insacc.myapplication.Model.BookingSkuItem;
import org.insacc.myapplication.Model.SmsRule;
import org.insacc.myapplication.MyApp;
import org.insacc.myapplication.R;
import org.insacc.myapplication.Service.ApplySmsRuleService.ApplySmsRuleReceiver;
import org.insacc.myapplication.SmsRules.SmsRuleActivity;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by can on 22.12.2016.
 */

public class AddSmsRuleActivity extends AppCompatActivity implements AddSmsRuleContract.View,
        BookingItemListDialog.OnItemsPickedListener, BookingStatusDialog.SelectedStatusCallback {

    @Inject
    AddSmsRuleContract.Presenter mPresenter;
    @BindView(R.id.add_sms_cancel_button)
    ImageButton mCancelButton;

    @BindView(R.id.add_new_rule_button)
    Button mAddSmsButton;

    @BindView(R.id.add_rule_name)
    EditText mRuleName;

    @BindView(R.id.current_time_picker)
    TimePicker mCurrentTimePicker;

    @BindView(R.id.plus_day_picker)
    NumberPicker mPlusDayPicker;

    @BindView(R.id.sms_rule_sms_text)
    EditText mSmsText;

    @BindView(R.id.add_rule_booking_contain_button)
    ImageButton mBookingContainButton;

    @BindView(R.id.add_rule_booking_not_contain_button)
    ImageButton mBookingNotContainButton;

    @BindView(R.id.add_rule_select_booking_w_group)
    RadioGroup mSelectBookingWith;

    @BindView(R.id.add_rule_check_in_status_radio)
    RadioGroup mCheckInStatusGroup;

    //@BindView(R.id.add_rule_user_received_sms_layout)
    //RadioGroup mUserReceivedRadioGroup;

    @BindView(R.id.add_rule_booking_status_group)
    RadioGroup mBookingStatusGroup;

    @BindView(R.id.add_rule_booking_status_radio_button)
    RadioButton mBookingCustomStatus;

    @BindView(R.id.add_rule_contains_list)
    RecyclerView mItemContainList;

    @BindView(R.id.add_rule_not_contain_list)
    RecyclerView mItemNotContainList;

    private boolean isEditRule;


    private List<BookingSkuItem> selectedContainItems;
    private List<BookingSkuItem> selectedNotContainItems;
    private String selectedBookingStatus;

    private LinearLayoutManager mContainListLayoutManager;
    private LinearLayoutManager mNotContainListLayoutManager;

    private InActivityItemListAdapter mContainListAdapter;
    private InActivityItemListAdapter mNotContainListAdapter;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        selectedContainItems = new ArrayList<>(0);
        selectedNotContainItems = new ArrayList<>(0);
        isEditRule = false;
        setContentView(R.layout.add_sms_rule);

        ButterKnife.bind(this);

        selectedBookingStatus = null;
        DaggerAddSmsRuleComponent.builder().addSmsRuleModule(new AddSmsRuleModule(this))
                .dbHelperModule(new DbHelperModule()).appComponent(((MyApp) getApplicationContext())
                .getAppComponent()).build().inject(this);


        mCancelButton.setOnClickListener(buttonListener());
        mBookingContainButton.setOnClickListener(buttonListener());
        mBookingNotContainButton.setOnClickListener(buttonListener());
        mBookingStatusGroup.setOnCheckedChangeListener(providesCheckButton());
        mAddSmsButton.setOnClickListener(buttonListener());

        mCurrentTimePicker.setIs24HourView(true);
        mPlusDayPicker.setMinValue(0);
        mPlusDayPicker.setMaxValue(50);


        mContainListLayoutManager = new LinearLayoutManager(this);
        mContainListLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mContainListAdapter = new InActivityItemListAdapter(new ArrayList<BookingSkuItem>(0), true, this);
        mItemContainList.setLayoutManager(mContainListLayoutManager);
        mItemContainList.setAdapter(mContainListAdapter);

        mNotContainListLayoutManager = new LinearLayoutManager(this);
        mNotContainListLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mNotContainListAdapter = new InActivityItemListAdapter(new ArrayList<BookingSkuItem>(0), false, this);
        mItemNotContainList.setLayoutManager(mNotContainListLayoutManager);
        mItemNotContainList.setAdapter(mNotContainListAdapter);

        if (getIntent().getExtras() != null) {
            if (getIntent().getExtras().getParcelable(Config.EDIT_SMS_RULE) != null) {
                SmsRule smsRule = (SmsRule) getIntent().getExtras().getParcelable(Config.EDIT_SMS_RULE);
                isEditRule = true;
                setEditSmsRuleFields(smsRule);

            }
        }
    }

    private void setEditSmsRuleFields(SmsRule smsRule) {
        this.selectedContainItems = smsRule.getBookingContainItems();
        this.selectedNotContainItems = smsRule.getBookingNotContainItems();
        this.selectedBookingStatus = smsRule.getmBookingStatus();
        if (selectedContainItems != null)
            mContainListAdapter.updateAllSkuList(selectedContainItems);

        if (selectedNotContainItems != null)
            mNotContainListAdapter.updateAllSkuList(selectedNotContainItems);
        mBookingStatusGroup.setOnCheckedChangeListener(null);
        if (selectedBookingStatus == (null)) {

            mBookingStatusGroup.check(R.id.add_rule_booking_status_does_not_matter);
        } else {
            mBookingStatusGroup.check(R.id.add_rule_booking_status_radio_button);
            mBookingCustomStatus.setText(selectedBookingStatus);
        }
        mBookingStatusGroup.setOnCheckedChangeListener(providesCheckButton());
        mSmsText.setText(smsRule.getSmsToSend());
        mRuleName.setText(smsRule.getRuleName());
        ((RadioButton) mCheckInStatusGroup.getChildAt(smsRule.getCheckInOutStatus())).setChecked(true);
        mPlusDayPicker.setValue(smsRule.getAdditionalDayCount());
        int startToday = smsRule.isStartDayToday() == true ? 0 : 1;
        ((RadioButton) mSelectBookingWith.getChildAt(startToday)).setChecked(true);

        mCurrentTimePicker.setCurrentHour(Integer.parseInt(smsRule.getmCurrentHour()));
        mCurrentTimePicker.setCurrentMinute(Integer.parseInt(smsRule.getmCurrentMinute()));

        mRuleName.setEnabled(false);


    }

    private RadioGroup.OnCheckedChangeListener providesCheckButton() {

        return new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int checkedId) {

                if (checkedId == R.id.add_rule_booking_status_radio_button) {
                    mPresenter.callBookingStatusListUi();

                } else if (checkedId == R.id.add_rule_booking_status_does_not_matter) {
                    selectedBookingStatus = null;
                }
            }
        };
    }

    private View.OnClickListener buttonListener() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                switch (view.getId()) {

                    case R.id.add_sms_cancel_button:
                        mPresenter.callSmsRulesUi();
                        break;

                    case R.id.add_new_rule_button:

                        mPresenter.saveSmsRule(collectRuleInformation(), isEditRule);

                        break;

                    case R.id.add_rule_booking_contain_button:
                        mPresenter.callAddNewContainItem();
                        break;

                    case R.id.add_rule_booking_not_contain_button:
                        mPresenter.callAddNewNotContainItem();
                        break;
                }
            }
        };
    }

    private SmsRule collectRuleInformation() {

        SmsRule smsRule = new SmsRule();
        String currentHour, currentMinute;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
            currentHour = String.valueOf(mCurrentTimePicker.getHour());
            currentMinute = String.valueOf(mCurrentTimePicker.getMinute());
        } else {
            currentHour = String.valueOf(mCurrentTimePicker.getCurrentHour());
            currentMinute = String.valueOf(mCurrentTimePicker.getCurrentMinute());
        }

        String plusDayCount = String.valueOf(mPlusDayPicker.getValue());
        String ruleName = mRuleName.getText().toString();
        String smsText = mSmsText.getText().toString();


        if (!inputChecks(ruleName, smsText)) {
            return null;
        }

        boolean isBookingEndDayToday = isBookingSelectedToday();
        boolean isUserReceivedSms = true;//= isUserReceivedSms();
        int checkInOutStatus = getCheckInStatus();
        String bookingStatus = getBookingStatus();

        smsRule.setAdditionalDayCount(Integer.parseInt(plusDayCount));
        smsRule.setRuleName(ruleName);
        smsRule.setSmsToSend(smsText);
        smsRule.setStartDayToday(isBookingEndDayToday);
        //smsRule.setReceivedSms(isUserReceivedSms);
        smsRule.setmBookingStatus(bookingStatus);
        smsRule.setCheckInOutStatus(checkInOutStatus);
        smsRule.setBookingContainItems(selectedContainItems);
        smsRule.setBookingNotContainItems(selectedNotContainItems);
        smsRule.setmCurrentMinute(currentMinute);
        smsRule.setmCurrentHour(currentHour);


        return smsRule;

    }

    private boolean inputChecks(String ruleName, String text) {
        if (ruleName == null || ruleName.length() == 0
                || text == null || text.length() == 0) {
            return false;
        }


        return true;
    }

    private boolean isBookingSelectedToday() {
        int checkedRadioButton = mSelectBookingWith.getCheckedRadioButtonId();
        View radioButton = mSelectBookingWith.findViewById(checkedRadioButton);
        int selectedBooking = mSelectBookingWith.indexOfChild(radioButton);

        if (selectedBooking == 0) {
            return true;
        } else {
            return false;
        }
    }

    /*private boolean isUserReceivedSms() {


        int checkedRadioButton = mUserReceivedRadioGroup.getCheckedRadioButtonId();
        View radioButton = mUserReceivedRadioGroup.findViewById(checkedRadioButton);
        int selectedBooking = mUserReceivedRadioGroup.indexOfChild(radioButton);

        if (selectedBooking == 0) {
            return true;
        } else {
            return false;
        }
    }*/

    private int getCheckInStatus() {
        int checkedRadioButtonId = mCheckInStatusGroup.getCheckedRadioButtonId();
        View checkedButton = mCheckInStatusGroup.findViewById(checkedRadioButtonId);
        int selectedIndex = mCheckInStatusGroup.indexOfChild(checkedButton);

        return selectedIndex;
    }

    private String getBookingStatus() {
        return selectedBookingStatus;
    }

    @Override
    public void showSmsRuleSavedMsg() {
        Toast.makeText(this, getString(R.string.add_sms_rule_successfully), Toast.LENGTH_LONG).show();
    }

    @Override
    public void showSmsRuleSaveFail() {
        Toast.makeText(this, getString(R.string.add_sms_rule_fail), Toast.LENGTH_LONG).show();
    }

    @Override
    public void openSmsRulesUi() {
        startActivity(new Intent(this, SmsRuleActivity.class));
    }

    @Override
    public void openNewContainItemUi() {
        BookingItemListDialog bookingItemListDialog = BookingItemListDialog.newInstance(true);
        bookingItemListDialog.show(getSupportFragmentManager(), "ContainedItemsDialog");
    }

    @Override
    public void openNewNotContainItemUi() {
        BookingItemListDialog bookingItemListDialog = BookingItemListDialog.newInstance(false);
        bookingItemListDialog.show(getSupportFragmentManager(), "NotContainedItems");
    }

    @Override
    public void openBookingStatusListUi() {
        BookingStatusDialog.newInstance().show(getSupportFragmentManager(), "BookingStatusDialog");
    }

    @Override
    public void createAlarmForRule(SmsRule smsRule) {

        AlarmManager alarmManager = (AlarmManager) this.getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(this, ApplySmsRuleReceiver.class);
        intent.putExtra(Config.SMS_RULE_ID, smsRule.getId());
        PendingIntent pendingIntent = PendingIntent.getBroadcast(getApplicationContext(),
                (int) smsRule.getId(), intent, 0);


        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(System.currentTimeMillis());
        calendar.set(Calendar.HOUR_OF_DAY, Integer.parseInt(smsRule.getmCurrentHour()));
        calendar.set(Calendar.MINUTE, Integer.parseInt(smsRule.getmCurrentMinute()));


        alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(),
                AlarmManager.INTERVAL_DAY, pendingIntent);
    }

    @Override
    public void fillAllFieldsMsg() {
        Toast.makeText(this, getString(R.string.fill_all_fields), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void removeContainItem(int position) {
        selectedContainItems.remove(position);
    }

    @Override
    public void removeNotContainItem(int position) {
        selectedNotContainItems.remove(position);
    }

    @Override
    public void editAlarmForRule(SmsRule smsRule) {

        AlarmManager alarmManager = (AlarmManager) this.getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(this, ApplySmsRuleReceiver.class);
        intent.putExtra(Config.SMS_RULE_ID, smsRule.getId());
        PendingIntent pendingIntent = PendingIntent.getBroadcast(getApplicationContext(),
                (int) smsRule.getId(), intent, PendingIntent.FLAG_UPDATE_CURRENT);

        // dIT HE  IK GEDAAN EN DE FLAG UPDATE CURRECT GEZET
        //alarmManager.cancel(pendingIntent);

        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(System.currentTimeMillis());
        calendar.set(Calendar.HOUR_OF_DAY, Integer.parseInt(smsRule.getmCurrentHour()));
        calendar.set(Calendar.MINUTE, Integer.parseInt(smsRule.getmCurrentMinute()));


        alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(),
                AlarmManager.INTERVAL_DAY, pendingIntent);
    }


    @Override
    public void onItemsPicked(BookingSkuItem selectedItems) {
        if (selectedContainItems == null)
            selectedContainItems = new ArrayList<>();

        if (selectedNotContainItems == null)
            selectedNotContainItems = new ArrayList<>();
        selectedContainItems.add(selectedItems);
        mContainListAdapter.updateSkuList(selectedItems);
        mItemContainList.invalidate();

    }

    @Override
    public void onNotContainedItemsPicked(BookingSkuItem selectedNotContainItem) {
        this.selectedNotContainItems.add(selectedNotContainItem);
        mNotContainListAdapter.updateSkuList(selectedNotContainItem);
        mItemNotContainList.invalidate();
    }

    @Override
    public void onStatusSelected(String name) {
        //TODO handle doesnot matter choice
        mBookingCustomStatus.setText(name);
        this.selectedBookingStatus = name;
    }
}
