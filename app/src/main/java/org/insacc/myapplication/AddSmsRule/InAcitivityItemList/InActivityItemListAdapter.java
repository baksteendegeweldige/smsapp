package org.insacc.myapplication.AddSmsRule.InAcitivityItemList;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.daimajia.swipe.SwipeLayout;
import com.daimajia.swipe.adapters.RecyclerSwipeAdapter;

import org.insacc.myapplication.AddSmsRule.AddSmsRuleContract;
import org.insacc.myapplication.Model.BookingSkuItem;
import org.insacc.myapplication.R;

import java.util.List;

/**
 * Created by can on 28.01.2017.
 */

public class InActivityItemListAdapter extends RecyclerSwipeAdapter<InActivityItemListAdapter
        .InActivityViewHolder> {

    private List<BookingSkuItem> mSkuItems;

    private AddSmsRuleContract.View mView;

    private boolean isSelectedType;

    private Context mContext;

    public InActivityItemListAdapter(List<BookingSkuItem> skuItems, boolean isSelectedType,
                                     AddSmsRuleContract.View view) {
        this.mSkuItems = skuItems;
        this.isSelectedType = isSelectedType;
        this.mView = view;
    }

    public void updateSkuList(BookingSkuItem skuItem) {
        mSkuItems.add(skuItem);
        notifyDataSetChanged();
    }

    public void updateAllSkuList(List<BookingSkuItem> skuItems) {
        mSkuItems.addAll(skuItems);
        notifyDataSetChanged();
    }

    @Override
    public InActivityViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View root = LayoutInflater.from(parent.getContext()).inflate(R.layout.in_activity_item_list_item,
                parent, false);
        mContext = parent.getContext();
        return new InActivityViewHolder(root);
    }

    private void openAlertDialog(int position) {
        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(mContext);
        alertDialogBuilder.setMessage(mSkuItems.get(position).toString());


        alertDialogBuilder.setNeutralButton(mContext.getString(R.string.close_tag), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    @Override
    public void onBindViewHolder(final InActivityViewHolder holder, final int position) {
        holder.mItemTag.setText("OR " + mSkuItems.get(position).toString());


        holder.mDeleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isSelectedType) {
                    mView.removeContainItem(position);
                } else {
                    mView.removeNotContainItem(position);
                }

                mItemManger.removeShownLayouts(holder.mSwipeLayout);
                mSkuItems.remove(position);
                notifyItemRemoved(position);
                notifyItemRangeChanged(position, mSkuItems.size());
                mItemManger.closeAllItems();
            }
        });


        holder.mSwipeLayout.addSwipeListener(new SwipeLayout.SwipeListener() {
            @Override
            public void onStartOpen(SwipeLayout layout) {

            }

            @Override
            public void onOpen(SwipeLayout layout) {

            }

            @Override
            public void onStartClose(SwipeLayout layout) {

            }

            @Override
            public void onClose(SwipeLayout layout) {

            }

            @Override
            public void onUpdate(SwipeLayout layout, int leftOffset, int topOffset) {

            }

            @Override
            public void onHandRelease(SwipeLayout layout, float xvel, float yvel) {

            }
        });

        holder.mInActivityButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openAlertDialog(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mSkuItems.size();
    }

    @Override
    public int getSwipeLayoutResourceId(int position) {
        return R.id.in_activity_item_swipe_layout;
    }

    public static class InActivityViewHolder extends RecyclerView.ViewHolder {

        private TextView mItemTag;
        private SwipeLayout mSwipeLayout;
        private TextView mDeleteButton;
        private RelativeLayout mInActivityButton;

        public InActivityViewHolder(View itemView) {
            super(itemView);
            mSwipeLayout = (SwipeLayout) itemView.findViewById(R.id.in_activity_item_swipe_layout);
            mDeleteButton = (TextView) itemView.findViewById(R.id.in_activity_item_delete);
            mItemTag = (TextView) itemView.findViewById(R.id.in_activity_item_tag);
            mInActivityButton = (RelativeLayout) itemView.findViewById(R.id.in_activity_button_layout);
        }
    }
}
