package org.insacc.myapplication.SmsRules;

import android.Manifest;
import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.security.ProviderInstaller;

import org.insacc.myapplication.AddSmsRule.AddSmsRuleActivity;
import org.insacc.myapplication.Config;
import org.insacc.myapplication.Configuration.ConfigurationActivity;
import org.insacc.myapplication.DaggerModules.DbHelperModule;
import org.insacc.myapplication.DaggerModules.SmsRulesModule.DaggerSmsRulesComponent;
import org.insacc.myapplication.DaggerModules.SmsRulesModule.SmsRulesModule;
import org.insacc.myapplication.Model.SmsRule;
import org.insacc.myapplication.MyApp;
import org.insacc.myapplication.R;
import org.insacc.myapplication.Service.ApplySmsRuleService.ApplySmsRuleReceiver;
import org.insacc.myapplication.Service.ApplySmsRuleService.ApplySmsRuleService;
import org.insacc.myapplication.Service.RebootRecoverService.RebootRecoverService;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SmsRuleActivity extends AppCompatActivity implements SmsRulesContract.View {

    @BindView(R.id.add_rule_button)
    Button mAddRuleButton;

    @BindView(R.id.config_button)
    Button mConfigButton;

    @BindView(R.id.sms_rule_toolbar)
    Toolbar mToolbar;

    @BindView(R.id.toolbar_left_icon)
    ImageButton mToolbarLeftButton;

    @BindView(R.id.toolbar_title)
    TextView mToolbarTitle;

    @BindView(R.id.sms_rule_recycler_view)
    RecyclerView mSmsRuleList;


    @Inject
    SmsRulesContract.Presenter mPresenter;

    private SmsRuleListAdapter mListAdapter;
    private LinearLayoutManager mLayoutManager;

    private static final int SEND_SMS_PERMISSION_REQ_CODE = 1;
    private static final int READ_SMS_PERMISSION_REQ_CODE = 2;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        updateAndroidSecurityProvider(this);

        ButterKnife.bind(this);
        ButterKnife.bind(mToolbar, this);

        setToolbarDetails();

        DaggerSmsRulesComponent.builder().smsRulesModule(new SmsRulesModule(this)).dbHelperModule(new DbHelperModule())
                .appComponent(((MyApp) getApplicationContext()).getAppComponent()).build().inject(this);

        mConfigButton.setOnClickListener(optionButtonListener());
        mAddRuleButton.setOnClickListener(optionButtonListener());

        mLayoutManager = new LinearLayoutManager(this);
        mSmsRuleList.setLayoutManager(mLayoutManager);
        mListAdapter = new SmsRuleListAdapter(new ArrayList<SmsRule>(0), this);
        mSmsRuleList.setAdapter(mListAdapter);

        mPresenter.callPopulateSmsRuleList();


    }

    private void updateAndroidSecurityProvider(Activity callingActivity) {
        try {
            ProviderInstaller.installIfNeeded(this);
        } catch (GooglePlayServicesRepairableException e) {
            // Thrown when Google Play Services is not installed, up-to-date, or enabled
            // Show dialog to allow users to install, update, or otherwise enable Google Play services.
            GooglePlayServicesUtil.getErrorDialog(e.getConnectionStatusCode(), callingActivity, 0);
        } catch (GooglePlayServicesNotAvailableException e) {
            Log.e("SecurityException", "Google Play Services not available.");
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        mPresenter.checkSmsReadPermission();
    }

    private View.OnClickListener optionButtonListener() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                switch (view.getId()) {

                    case R.id.add_rule_button:
                        mPresenter.callAddSmsRuleView();
                        break;

                    case R.id.config_button:
                        mPresenter.callConfigView();
                        break;
                }
            }
        };
    }

    private void setToolbarDetails() {
        this.mToolbarTitle.setText(getString(R.string.sms_rule_title));
        mToolbarLeftButton.setVisibility(View.GONE);
    }

    @Override
    public void openAddSmsRuleUi() {
        startActivity(new Intent(this, AddSmsRuleActivity.class));
    }

    @Override
    public void openConfigView() {
       /* if (isSmsSendAllowed()) {
            Intent intent = new Intent(this, ApplySmsRuleService.class);
            intent.putExtra(Config.SMS_RULE_ID, 4);
            startService(intent);
        } else {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.SEND_SMS},
                    SEND_SMS_PERMISSION_REQ_CODE);
        }*/


        startActivity(new Intent(this, ConfigurationActivity.class));

    }

    @Override
    public void populateSmsRuleList(List<SmsRule> smsRules) {

        mListAdapter.updateList(smsRules);
    }

    @Override
    public void deleteSmsRule(SmsRule smsRule) {
        mPresenter.callDeleteRule(smsRule);
    }

    @Override
    public void displaySmsRuleFailMsg() {

    }

    @Override
    public void checkSmsPermission() {

        if (!isSmsReadAllowed()) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_SMS},
                    SEND_SMS_PERMISSION_REQ_CODE);
        }


    }

    @Override
    public void disableDeleteRule(long ruleId) {
        AlarmManager alarmManager = (AlarmManager) this.getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(this, ApplySmsRuleReceiver.class);
        //ntent.putExtra(Config.SMS_RULE_ID, smsRule.getId());
        PendingIntent pendingIntent = PendingIntent.getBroadcast(getApplicationContext(),
                (int) ruleId, intent, 0);

        alarmManager.cancel(pendingIntent);

    }

    @Override
    public void openEditSmsRuleUi(SmsRule smsRule) {
        Intent intent = new Intent(this, AddSmsRuleActivity.class);
        intent.putExtra(Config.EDIT_SMS_RULE, smsRule);
        startActivity(intent);
    }


    @Override
    public void openEditSmsRuleClicked(SmsRule smsRule) {
        mPresenter.callEditSmsRuleUi(smsRule);

    }

    private boolean isSmsReadAllowed() {
        return (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_SMS)
                == PackageManager.PERMISSION_GRANTED);

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        switch (requestCode) {
            case SEND_SMS_PERMISSION_REQ_CODE:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    mPresenter.callConfigView();
                } else {
                    Toast.makeText(this, getString(R.string.app_need_sms_permission),
                            Toast.LENGTH_SHORT).show();
                }
                break;
            case READ_SMS_PERMISSION_REQ_CODE:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    //Todo add ty msg
                } else {
                    Toast.makeText(this, getString(R.string.app_nee_sms_read_permission),
                            Toast.LENGTH_SHORT).show();
                }
        }
    }

    private boolean isSmsSendAllowed() {
        return (ContextCompat.checkSelfPermission(this, Manifest.permission.SEND_SMS)
                == PackageManager.PERMISSION_GRANTED);
    }
}
