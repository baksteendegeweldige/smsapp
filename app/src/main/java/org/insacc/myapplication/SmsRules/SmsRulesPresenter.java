package org.insacc.myapplication.SmsRules;

import android.util.Log;

import org.insacc.myapplication.Database.DeleteSmsRuleService.DeleteSmsRuleService;
import org.insacc.myapplication.Database.GetSmsRuleService.GetSmsRuleService;
import org.insacc.myapplication.Database.GetSmsRulesService.GetSmsRulesService;
import org.insacc.myapplication.Model.Booking;
import org.insacc.myapplication.Model.BookingContainerModel;
import org.insacc.myapplication.Model.BookingDetail;
import org.insacc.myapplication.Model.BookingDetailContainerModel;
import org.insacc.myapplication.Model.SmsRule;
import org.insacc.myapplication.Service.GetBookingService.GetBookingService;
import org.insacc.myapplication.Service.GetBookingsService.GetBookingsService;

import java.util.HashMap;
import java.util.List;

/**
 * Created by can on 22.12.2016.
 */

public class SmsRulesPresenter implements SmsRulesContract.Presenter,
        GetBookingService.GetBookingCallback, GetBookingsService.GetBookingsCallback,
        GetSmsRulesService.GetSmsRulesCallback, DeleteSmsRuleService.SmsRuleDeleteCallback {

    private SmsRulesContract.View mView;

    private GetSmsRulesService mSmsRuleService;

    private DeleteSmsRuleService mDeleteSmsRuleService;

    //private GetBookingService mGetBookingService;

    //private GetBookingsService mGetBookingsService;

    public SmsRulesPresenter(SmsRulesContract.View view, GetSmsRulesService getSmsRulesService,
                             DeleteSmsRuleService deleteSmsRuleService) {
        this.mView = view;
        this.mDeleteSmsRuleService = deleteSmsRuleService;
        this.mSmsRuleService = getSmsRulesService;
        //   this.mGetBookingsService = getBookingsService;
        // this.mGetBookingService = getBookingService;
    }

    @Override
    public void callAddSmsRuleView() {
        mView.openAddSmsRuleUi();
    }

    @Override
    public void callConfigView() {
        mView.openConfigView();
        //mGetBookingService.getBooking("FFGY-250117", this);
        //mGetBookingsService.getBookings(new HashMap<String, Object>(), this);
    }

    @Override
    public void callPopulateSmsRuleList() {
        mSmsRuleService.getSmsRules(this);
    }

    @Override
    public void callDeleteRule(SmsRule smsRule) {

        mDeleteSmsRuleService.deleteSmsRule(smsRule.getId(), this);

    }

    @Override
    public void checkSmsReadPermission() {
        mView.checkSmsPermission();
    }

    @Override
    public void callEditSmsRuleUi(SmsRule smsRule) {
        mView.openEditSmsRuleUi(smsRule);
    }

    @Override
    public void onBookingLoaded(BookingDetail booking) {
        Log.i("bookingDetail", booking.getItems().get("1").getName());

    }

    @Override
    public void onBookingLoadFailed() {
        Log.i("bookingDetailFail", "called");
    }

    @Override
    public void onBookingsLoaded(BookingContainerModel bookingContainerModel) {
        Log.i("firstBooking", bookingContainerModel.getBookingIndex().get("1").getCustomerEmail());


    }

    @Override
    public void onBookingLoadFail() {
        Log.i("bookingFail", "called");
    }

    @Override
    public void onSmsRulesLoaded(List<SmsRule> smsRuleList) {
        mView.populateSmsRuleList(smsRuleList);
        //Hier moeten ze ook nog allemaal geladen worden denk ik
    }

    @Override
    public void onSmsRuleLoadFailed() {
        Log.i("bookingFail", "called");
        mView.displaySmsRuleFailMsg();
    }

    @Override
    public void onSmsRuleDeleted(long deletedSmsId) {
        //TODO display rule deleted
        mView.disableDeleteRule(deletedSmsId);
    }

    @Override
    public void onSmsRuleDeleteFail() {
        //TODO display rule cannot be deleted
    }
}
