package org.insacc.myapplication.SmsRules;

import org.insacc.myapplication.Model.SmsRule;

import java.util.List;

/**
 * Created by can on 22.12.2016.
 */

public interface SmsRulesContract {

    interface View {
        void openAddSmsRuleUi();

        void openConfigView();

        void populateSmsRuleList(List<SmsRule> smsRules);

        void deleteSmsRule(SmsRule smsRule);

        void displaySmsRuleFailMsg();

        void checkSmsPermission();

        void disableDeleteRule(long ruleId);

        void openEditSmsRuleClicked(SmsRule smsRule);

        void openEditSmsRuleUi(SmsRule smsRule);
    }

    interface Presenter {
        void callAddSmsRuleView();

        void callConfigView();

        void callPopulateSmsRuleList();

        void callDeleteRule(SmsRule smsRule);

        void checkSmsReadPermission();

        void callEditSmsRuleUi(SmsRule smsRule);
    }
}
