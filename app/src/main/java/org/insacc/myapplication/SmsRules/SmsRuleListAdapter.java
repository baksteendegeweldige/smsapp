package org.insacc.myapplication.SmsRules;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.daimajia.swipe.SwipeLayout;
import com.daimajia.swipe.adapters.RecyclerSwipeAdapter;

import org.insacc.myapplication.Model.SmsRule;
import org.insacc.myapplication.R;

import java.util.List;

/**
 * Created by can on 27.12.2016.
 */

public class SmsRuleListAdapter extends RecyclerSwipeAdapter<SmsRuleListAdapter.CustomViewHolder> {


    private List<SmsRule> mSmsRules;

    private SmsRulesContract.View mView;

    public SmsRuleListAdapter(List<SmsRule> smsRules, SmsRulesContract.View view) {
        this.mSmsRules = smsRules;
        this.mView = view;
    }

    @Override
    public int getSwipeLayoutResourceId(int position) {
        return R.id.sms_list_item_swipe_layout;
    }

    public static class CustomViewHolder extends RecyclerView.ViewHolder {

        TextView mSmsRuleName;

        private LinearLayout mBehindView;
        private SwipeLayout mSwipeLayout;
        private TextView mDeleteButton;
        private Button mEditButton;
        private TextView mSmsRuleTime;

        public CustomViewHolder(View itemView) {
            super(itemView);
            mSmsRuleName = (TextView) itemView.findViewById(R.id.sms_rule_list_name);
            mBehindView = (LinearLayout) itemView.findViewById(R.id.sms_rule_list_behind_view);
            mSwipeLayout = (SwipeLayout) itemView.findViewById(R.id.sms_list_item_swipe_layout);
            mDeleteButton = (TextView) itemView.findViewById(R.id.sms_rule_list_item_delete);
            mEditButton = (Button) itemView.findViewById(R.id.sms_rule_item_edit);
            mSmsRuleTime = (TextView) itemView.findViewById(R.id.sms_rule_item_time);
        }
    }


    @Override
    public CustomViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.sms_rule_list_item,
                parent, false);
        return new CustomViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final CustomViewHolder holder, final int position) {
        final int listPosition = position;
        holder.mSmsRuleName.setText(mSmsRules.get(position).getRuleName());
        holder.mSmsRuleTime.setText(mSmsRules.get(position).getmCurrentHour() + " : "
                + mSmsRules.get(position).getmCurrentMinute());

        holder.mDeleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mView.deleteSmsRule(mSmsRules.get(listPosition));
                mItemManger.removeShownLayouts(holder.mSwipeLayout);
                mSmsRules.remove(listPosition);
                notifyItemRemoved(listPosition);
                notifyItemRangeChanged(listPosition, mSmsRules.size());
                mItemManger.closeAllItems();
            }
        });


        holder.mSwipeLayout.addSwipeListener(new SwipeLayout.SwipeListener() {
            @Override
            public void onStartOpen(SwipeLayout layout) {

            }

            @Override
            public void onOpen(SwipeLayout layout) {

            }

            @Override
            public void onStartClose(SwipeLayout layout) {

            }

            @Override
            public void onClose(SwipeLayout layout) {

            }

            @Override
            public void onUpdate(SwipeLayout layout, int leftOffset, int topOffset) {

            }

            @Override
            public void onHandRelease(SwipeLayout layout, float xvel, float yvel) {

            }
        });

        holder.mEditButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mView.openEditSmsRuleClicked(mSmsRules.get(position));
            }
        });

    }


    @Override
    public int getItemCount() {
        return mSmsRules.size();
    }

    public void updateList(List<SmsRule> smsRules) {
        this.mSmsRules = smsRules;
        notifyDataSetChanged();
    }
}
