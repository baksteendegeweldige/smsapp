package org.insacc.myapplication.Service;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import org.insacc.myapplication.Model.BookingDetail;
import org.insacc.myapplication.Model.Item;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;


/**
 * Created by can on 1.02.2017.
 */

public class GsonCustomConverter implements JsonDeserializer<BookingDetail> {


    @Override
    public BookingDetail deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {

        JsonObject jsonObject = (JsonObject) json;
        Gson gson = new Gson();

        JsonObject bookingsObject = jsonObject.getAsJsonObject("booking");
        Log.i("bookingObject", bookingsObject.toString());

        BookingDetail bookingDetail = gson.fromJson(bookingsObject, BookingDetail.class);
        Log.i("bookingDetailString", bookingDetail.toString());
        Log.i("bookingDetailItems", bookingDetail.getItems().toString());
        //JsonObject itemsObject = bookingsObject.getAsJsonObject("items");
        //JsonElement checkIn = bookingsObject.get("checkin");
        /*if(checkIn != null) {
            Log.i("itemsObject", itemsObject.toString());
            boolean hasNext = true;
            int itemIndex = 1;
            List<Item> items = new ArrayList<>();
            while (hasNext) {
                JsonObject item = itemsObject.getAsJsonObject(String.valueOf(itemIndex));

                if (item != null) {
                    Item tempItem = gson.fromJson(item, Item.class);
                    items.add(tempItem);
                    itemIndex++;
                } else {
                    hasNext = false;
                }

            }*/



            //BookingDetail bookingDetail = gson.fromJson(bookingsObject, BookingDetail.class);
          //  bookingDetail.setItems(items);
            return bookingDetail;
      /*  } else {
            return null;
        }*/



    }
}
