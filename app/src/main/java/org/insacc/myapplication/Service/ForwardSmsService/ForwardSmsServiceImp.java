package org.insacc.myapplication.Service.ForwardSmsService;

import android.app.Service;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.telephony.SmsManager;
import android.util.Log;

import org.insacc.myapplication.Config;
import org.insacc.myapplication.Database.CheckCustInfoService.CheckCustInfoService;
import org.insacc.myapplication.Database.CheckCustInfoService.CheckCustInfoServiceImp;
import org.insacc.myapplication.Database.DatabaseHelper;
import org.insacc.myapplication.Model.CustomerInformation;
import org.insacc.myapplication.R;
import org.insacc.myapplication.Service.GetCustomerInfoService.GetCustomerInfoService;
import org.insacc.myapplication.Service.GetCustomerInfoService.GetCustomerInfoServiceImp;
import org.insacc.myapplication.Util.BelgianNumberCheck;

import rx.Subscription;

/**
 * Created by can on 22.12.2016.
 */

public class ForwardSmsServiceImp extends Service implements CheckCustInfoService.CheckCustInfoCallback {

    Subscription mSubscription;

    private BelgianNumberCheck mNumberCheck;

    private String mSmsText;

    private String mPhoneNumber;

    private SmsManager mSmsManager;

    private DatabaseHelper mDbHelper;

    private SharedPreferences mSharedPreferences;

    private ForwardMailHelper mForwardMailHelper;

    private CheckCustInfoServiceImp mCheckCustInfoService;

    private String mUsername;


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        mUsername = mSharedPreferences.getString(Config.EMAIL_ADDRESS, "forjusttestpurposes@gmail.com");
        String emailPassword = mSharedPreferences.getString(Config.EMAIL_PASSWORD, "test_1234");

        if (emailPassword != null && mUsername != null) {
            mForwardMailHelper = new ForwardMailHelper(mUsername, emailPassword);

        }

        if (intent.getExtras() != null) {
            Log.i("sendEmail", "called");
            mSmsText = intent.getExtras().getString(Config.RECEIVED_SMS_TEXT);
            Log.i("sendEmail", mSmsText);
            mPhoneNumber = intent.getExtras().getString(Config.RECEIVED_SMS_NUMBER);
            Log.i("sendEmail", mPhoneNumber);
            Log.i("forwardMail", String.valueOf(mForwardMailHelper != null));
            if (mNumberCheck.isBelgianNumber(mPhoneNumber) && mForwardMailHelper != null) {
                mCheckCustInfoService.checkCustomerInfo(mPhoneNumber, this);
                Log.i("sendEmail", "calledAfter");

                //mSmsManager.sendTextMessage(mPhoneNumber, null, mSmsText, null, null);
            } else {

            }
        }


        return START_STICKY;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Log.i("forwardService", "created");
        mSmsManager = SmsManager.getDefault();
        mNumberCheck = new BelgianNumberCheck();
        mDbHelper = DatabaseHelper.getInstance(getApplicationContext());
        mCheckCustInfoService = new CheckCustInfoServiceImp(mDbHelper);
        mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);


    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCustomerInfoLoaded(CustomerInformation customerInformation) {

        mForwardMailHelper.set_subject(getString(R.string.sms_received)
                + String.valueOf(mPhoneNumber));
        mForwardMailHelper.set_to(new String[]{mUsername});
        mForwardMailHelper.setBody(customerInformation.getCustomerName() + " " + customerInformation.getPhoneNumber()
                + " " + customerInformation.getReservationID());

        try {
            mForwardMailHelper.sendMailOnBackground();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onCustomerInfoNotFound() {

        mForwardMailHelper.set_subject(getString(R.string.sms_received)
                + String.valueOf(mPhoneNumber));
        mForwardMailHelper.set_to(new String[]{mUsername});
        mForwardMailHelper.set_from(mUsername);
        mForwardMailHelper.setBody("Test");

        try {
            mForwardMailHelper.sendMailOnBackground();

        } catch (Exception e) {
            Log.i("sendError", e.toString());
            e.printStackTrace();
        }
    }
}
