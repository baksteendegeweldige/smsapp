package org.insacc.myapplication.Service;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.insacc.myapplication.Config;
import org.insacc.myapplication.Model.BookingDetail;
import org.insacc.myapplication.Service.Interceptors.RequestInterceptor;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by can on 8.01.2017.
 */

public class ApiWrapper {

    private static ApiCall retrofitApiCall = null;

    private static RequestInterceptor mRequestInterceptor;


    private ApiWrapper(RequestInterceptor interceptor) {
        this.mRequestInterceptor = interceptor;
        createRetrofit();
    }

    public static ApiCall getInstance(RequestInterceptor interceptor) {
        if (retrofitApiCall == null) {
            new ApiWrapper(interceptor);
            Log.i("retrofitNewInstance", "called");
        }

        return retrofitApiCall;
    }

    private void createRetrofit() {


        Log.i("retrofitCreateNew", "called");
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();

        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(mRequestInterceptor);
        httpClient.addInterceptor(logging);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Config.BASE_URL)
                .addConverterFactory(buildGsonConverter())
                .addConverterFactory(GsonConverterFactory.create())

                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .client(httpClient.build())
                .build();

        retrofitApiCall = retrofit.create(ApiCall.class);


    }

    private static GsonConverterFactory buildGsonConverter() {
        GsonBuilder gsonBuilder = new GsonBuilder();

        // Adding custom deserializers
        gsonBuilder.registerTypeAdapter(BookingDetail.class, new GsonCustomConverter());
        Gson myGson = gsonBuilder.create();

        return GsonConverterFactory.create(myGson);
    }

    public static void deleteRetrofitInstance() {
        retrofitApiCall = null;
    }
}
