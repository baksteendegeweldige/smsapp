package org.insacc.myapplication.Service.RebootRecoverService.RebootRecoverHelperService;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import org.insacc.myapplication.Config;
import org.insacc.myapplication.Database.GetSmsRulesService.GetSmsRulesService;
import org.insacc.myapplication.Model.SmsRule;
import org.insacc.myapplication.Service.ApplySmsRuleService.ApplySmsRuleReceiver;

import java.util.Calendar;
import java.util.List;

/**
 * Created by can on 13.02.2017.
 */

public class RebootRecoverHelperServiceImp implements RebootRecoverHelperService, GetSmsRulesService.GetSmsRulesCallback {

    private GetSmsRulesService mGetSmsRulesService;

    private Context mContext;

    private RebootRecoverHelperCallback mCallback;

    public RebootRecoverHelperServiceImp(GetSmsRulesService smsRulesService, Context context) {
        this.mGetSmsRulesService = smsRulesService;
        this.mContext = context;
    }

    @Override
    public void startAllRulesAfterReboot(RebootRecoverHelperCallback callback) {
        mGetSmsRulesService.getSmsRules(this);
        this.mCallback = callback;

    }

    @Override
    public void unSubscribe() {
        mGetSmsRulesService.unSubscribe();
    }

    private void startAllSmsRules(List<SmsRule> smsRules) {
        for (SmsRule smsRule : smsRules) {
            setAlarmForRule(smsRule);
        }
        mCallback.onRebootRecoverySucceed();
    }

    private void setAlarmForRule(SmsRule smsRule) {
        Log.i("rebootID", String.valueOf(smsRule.getId()));
        AlarmManager alarmManager = (AlarmManager) mContext.getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(mContext, ApplySmsRuleReceiver.class);
        intent.putExtra(Config.SMS_RULE_ID, smsRule.getId());
        PendingIntent pendingIntent = PendingIntent.getBroadcast(mContext, (int) smsRule.getId(),
                intent, 0);

        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(System.currentTimeMillis());
        calendar.set(Calendar.HOUR_OF_DAY, Integer.parseInt(smsRule.getmCurrentHour()));
        calendar.set(Calendar.MINUTE, Integer.parseInt(smsRule.getmCurrentMinute()));

        alarmManager.setInexactRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(),
                AlarmManager.INTERVAL_DAY, pendingIntent);
    }

    @Override
    public void onSmsRulesLoaded(List<SmsRule> smsRuleList) {
        startAllSmsRules(smsRuleList);
    }

    @Override
    public void onSmsRuleLoadFailed() {
        mCallback.onRebootRecoveryFailed();
    }
}
