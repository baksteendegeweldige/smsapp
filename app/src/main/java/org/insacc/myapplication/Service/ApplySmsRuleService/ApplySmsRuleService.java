package org.insacc.myapplication.Service.ApplySmsRuleService;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;
import android.widget.Toast;

import org.insacc.myapplication.Config;
import org.insacc.myapplication.Database.DatabaseHelper;
import org.insacc.myapplication.Database.GetSmsRuleService.GetSmsRuleService;
import org.insacc.myapplication.Database.GetSmsRuleService.GetSmsRuleServiceImp;
import org.insacc.myapplication.Database.SentSmsDataService.SentSmsDataService;
import org.insacc.myapplication.Database.SentSmsDataService.SentSmsDataServiceImp;
import org.insacc.myapplication.Model.Booking;
import org.insacc.myapplication.Model.BookingContainerModel;
import org.insacc.myapplication.Model.Item;
import org.insacc.myapplication.Model.SmsRule;
import org.insacc.myapplication.R;
import org.insacc.myapplication.Service.ApiCall;
import org.insacc.myapplication.Service.ApiWrapper;
import org.insacc.myapplication.Service.GetBookingService.GetBookingService;
import org.insacc.myapplication.Service.GetBookingService.GetBookingServiceImp;
import org.insacc.myapplication.Service.GetBookingsService.GetBookingsService;
import org.insacc.myapplication.Service.GetBookingsService.GetBookingsServiceImp;
import org.insacc.myapplication.Service.Interceptors.RequestInterceptor;
import org.insacc.myapplication.Service.SendSmsService.SendSmsService;
import org.insacc.myapplication.Service.SendSmsService.SendSmsServiceImp;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import rx.Observable;
import rx.Observer;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by can on 27.01.2017.
 */

public class ApplySmsRuleService extends Service implements GetSmsRuleService.GetSmsRuleCallback {

    private GetSmsRuleService mGetSmsRuleService;

    private GetBookingService mGetBookingService;

    private GetBookingsService mGetBookingsService;

    //private SentSmsDataService mSentDataService;

    private ApiCall mApiCall;

    private ApplySmsRuleApiHelper mApiHelper;

    private Intent mIntent;

    private DatabaseHelper mDbHelper;

    private Subscription mSubscription;

    private SendSmsService mSendSmsService;

    private BookingContainerModel mBookingContainer;


    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        long smsRuleId = intent.getExtras().getLong(Config.SMS_RULE_ID, -1);

        if (smsRuleId != -1) {

            this.mIntent = intent;
            mGetSmsRuleService.getSmsRule(smsRuleId, this);
        }


        return START_STICKY;


    }

    @Override
    public void onCreate() {
        super.onCreate();
        mApiCall = ApiWrapper.getInstance(new RequestInterceptor());
        mDbHelper = DatabaseHelper.getInstance(getApplicationContext());

        mGetSmsRuleService = new GetSmsRuleServiceImp(DatabaseHelper.getInstance(getApplicationContext()));
        mSendSmsService = new SendSmsServiceImp();
        mGetBookingService = new GetBookingServiceImp(mApiCall);
        mGetBookingsService = new GetBookingsServiceImp(mApiCall);
       // mSentDataService = new SentSmsDataServiceImp(mDbHelper);


    }

    @Override
    public void onDestroy() {
        mGetSmsRuleService.unSubscribe();
        super.onDestroy();

    }

    @Override
    public void onSmsRuleLoaded(SmsRule smsRule) {

        boolean isStartDayToday = smsRule.isStartDayToday();
        HashMap<String, Object> queryMap = new HashMap<>();
        //Log.i("applyRuleService", "called");
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DAY_OF_YEAR, smsRule.getAdditionalDayCount());
        if (isStartDayToday) {
            //  Log.i("applyRuleService", "called1");
            // String start_date = String.valueOf(new Date().getTime() / 1000);
            String startDate = String.valueOf(calendar.getTimeInMillis() / 1000);
            Log.i("startDate", startDate);
            //= String.valueOf(1368136800);//
            queryMap.put("start_date", startDate);
        } else {
            //Log.i("applyRuleService", "called2");
            // String end_date = String.valueOf(new Date().getTime() / 1000);
            String endDate = String.valueOf(calendar.getTimeInMillis() / 1000);
            Log.i("endDate", endDate);
            queryMap.put("end_date", endDate);
        }

        mApiHelper = new ApplySmsRuleApiHelper(mGetBookingsService, mGetBookingService, smsRule
                /*mSentDataService*/);
        // Komt hij hier ?
        mApiHelper.getBookings(queryMap);
        //mGetBookingsService.getBookings(queryMap, this);

        /*Observable<List<Booking>> getBookings = mApiCall.getBookings(queryMap)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());

        mSubscription = getBookings.subscribe(new Observer<List<Booking>>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onNext(List<Booking> bookings) {

            }
        });*/


        ApplySmsRuleReceiver.completeWakefulIntent(mIntent);
    }

    @Override
    public void onSmsRuleLoadFailed() {
        ApplySmsRuleReceiver.completeWakefulIntent(mIntent);
    }


    public void unSubscribe() {
        if (mSubscription != null && !mSubscription.isUnsubscribed())
            mSubscription.unsubscribe();
    }


}
