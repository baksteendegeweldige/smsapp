package org.insacc.myapplication.Service.SendSmsService;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.telephony.SmsManager;
import android.util.Log;

import org.insacc.myapplication.Database.SentSmsDataService.SentSmsDataService;

import java.util.ArrayList;

/**
 * Created by can on 31.01.2017.
 */

public class SendSmsServiceImp implements SendSmsService {


    private SmsManager mSmsManager;


    @Override
    public void sendSms(String smsText, String phoneNumber, String customerName, String email) {
        mSmsManager = SmsManager.getDefault();
        Log.i("phoneNumberSend", phoneNumber);

        ArrayList<String> msgArray = mSmsManager.divideMessage(checkNameEmailOptions(smsText, customerName, email));
        mSmsManager.sendMultipartTextMessage(phoneNumber, null, msgArray, null, null);

        //mSmsManager.sendTextMessage(phoneNumber, null, checkNameEmailOptions(smsText,
                //customerName, email), null, null);
    }


    private String checkNameEmailOptions(String smsText, String custName, String custEmail) {
        Log.i("custEmail", custEmail);
        Log.i("custName", custName);
        Log.i("smsText", smsText);
        smsText = smsText.replaceAll("\\$customer_name\\b", custName);
        smsText = smsText.replaceAll("\\$customer_email\\b", custEmail);
        Log.i("smsText", smsText);
        return smsText;

    }
}
