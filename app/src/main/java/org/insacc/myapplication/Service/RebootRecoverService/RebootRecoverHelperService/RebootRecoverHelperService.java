package org.insacc.myapplication.Service.RebootRecoverService.RebootRecoverHelperService;

/**
 * Created by can on 13.02.2017.
 */

public interface RebootRecoverHelperService {

    void startAllRulesAfterReboot(RebootRecoverHelperCallback callback);

    void unSubscribe();

    interface RebootRecoverHelperCallback {


        void onRebootRecoverySucceed();

        void onRebootRecoveryFailed();


    }
}
