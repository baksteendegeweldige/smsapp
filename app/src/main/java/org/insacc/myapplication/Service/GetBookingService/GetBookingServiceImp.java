package org.insacc.myapplication.Service.GetBookingService;

import android.util.Log;

import org.insacc.myapplication.Model.Booking;
import org.insacc.myapplication.Model.BookingDetail;
import org.insacc.myapplication.Model.BookingDetailContainerModel;
import org.insacc.myapplication.Service.ApiCall;

import rx.Observable;
import rx.Observer;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by can on 19.01.2017.
 */

public class GetBookingServiceImp implements GetBookingService {

    private ApiCall mApiCall;

    private Subscription mSubscription;

    public GetBookingServiceImp(ApiCall apiCall) {
        this.mApiCall = apiCall;
    }

    @Override
    public void getBooking(String bookingId, final GetBookingCallback callback) {

        Observable<BookingDetail> getBooking = mApiCall.getBooking(bookingId)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io());

        mSubscription = getBooking.subscribe(new Observer<BookingDetail>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {
                Log.i("bookingErrorMSg", e.toString());
                //Log.i("bookingDetailError", e.getMessage());
                callback.onBookingLoadFailed();
            }

            @Override
            public void onNext(BookingDetail booking) {
                Log.i("bookingLoaded", "called");
                callback.onBookingLoaded(booking);
            }
        });

    }

    @Override
    public void unSubscribe() {
        if (mSubscription != null && !mSubscription.isUnsubscribed())
            mSubscription.unsubscribe();
    }
}
