package org.insacc.myapplication.Service.CleanOldCustomerInfoService;

import android.database.sqlite.SQLiteDatabase;

import org.insacc.myapplication.Database.DatabaseHelper;

import java.util.Calendar;
import java.util.concurrent.Callable;

import rx.Single;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

/**
 * Created by can on 13.02.2017.
 */

public class ClearOldCustomerInfoServiceImp implements ClearOldCustomerInfoService {

    private Subscription mSubscription;

    private DatabaseHelper mDbHelper;

    public ClearOldCustomerInfoServiceImp(DatabaseHelper databaseHelper) {
        this.mDbHelper = databaseHelper;
    }


    @Override
    public void deleteOldDate(final ClearOldDateCallback deleteCallback) {
        final Calendar oneWeekEarlier = Calendar.getInstance();
        oneWeekEarlier.add(Calendar.DAY_OF_YEAR, -7);


        Single<Boolean> deleteOldData = Single.fromCallable(new Callable<Boolean>() {
            @Override
            public Boolean call() throws Exception {
                return deleteOldDataHelper(oneWeekEarlier.getTimeInMillis());
            }
        })
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread());

        mSubscription = deleteOldData.subscribe(new Action1<Boolean>() {
            @Override
            public void call(Boolean isDeleted) {
                if (isDeleted) {
                    deleteCallback.onOldDataCleared();
                } else {
                    deleteCallback.onOldDataClearFail();
                }
            }
        });

    }

    private boolean deleteOldDataHelper(long oneWeekEarlier) {
        SQLiteDatabase database = mDbHelper.getWritableDatabase();
        long result = -1;
        try {
            result = database.delete(DatabaseHelper.CUSTOMER_PHONE_TABLE, DatabaseHelper.CUSTOMER_INFO_DATE + "<"
                    , new String[]{String.valueOf(oneWeekEarlier)});
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            database.close();
        }

        return result != -1;
    }

    @Override
    public void unSubscribe() {
        if (mSubscription != null && !mSubscription.isUnsubscribed())
            mSubscription.unsubscribe();
    }
}
