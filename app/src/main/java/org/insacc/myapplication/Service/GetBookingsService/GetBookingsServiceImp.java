package org.insacc.myapplication.Service.GetBookingsService;

import android.util.Log;

import org.insacc.myapplication.Model.Booking;
import org.insacc.myapplication.Model.BookingContainerModel;
import org.insacc.myapplication.Service.ApiCall;

import java.util.HashMap;
import java.util.List;

import rx.Observable;
import rx.Observer;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by can on 19.01.2017.
 */

public class GetBookingsServiceImp implements GetBookingsService {

    private ApiCall mApiCall;

    private Subscription mSubscription;

    public GetBookingsServiceImp(ApiCall apiCall) {
        this.mApiCall = apiCall;
    }

    @Override
    public void unSubscribe() {
        if (mSubscription != null && !mSubscription.isUnsubscribed())
            mSubscription.unsubscribe();
    }

    @Override
    public void getBookings(HashMap<String, Object> queryMap, final GetBookingsCallback callback) {

        // Hij vraagt hij de boekings op
        Observable<BookingContainerModel> getBookings = mApiCall.getBookings(queryMap)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io());

        mSubscription = getBookings.subscribe(new Observer<BookingContainerModel>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {
                Log.i("bookingDetailFail", e.getMessage());
                callback.onBookingLoadFail();
            }

            @Override
            public void onNext(BookingContainerModel bContainer) {
                callback.onBookingsLoaded(bContainer);
            }
        });
    }
}
