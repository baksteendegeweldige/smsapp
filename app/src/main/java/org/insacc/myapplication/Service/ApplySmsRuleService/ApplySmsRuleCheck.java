package org.insacc.myapplication.Service.ApplySmsRuleService;

import android.app.PendingIntent;
import android.content.Intent;
import android.telephony.SmsManager;
import android.util.Log;

import org.insacc.myapplication.Database.SentSmsDataService.SentSmsDataService;
import org.insacc.myapplication.Database.SentSmsDataService.SentSmsDataServiceImp;
import org.insacc.myapplication.Model.BookingDetail;
import org.insacc.myapplication.Model.CustomerInformation;
import org.insacc.myapplication.Model.SmsRule;
import org.insacc.myapplication.Service.SendSmsService.SendSmsService;
import org.insacc.myapplication.Service.SendSmsService.SendSmsServiceImp;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by can on 8.02.2017.
 */

public class ApplySmsRuleCheck implements SentSmsDataService.SaveCustomerInfoCallback {

    private List<BookingDetail> mBookingDetailList;

    private SmsRule mSmsRule;

    private ApplySmsRuleItemHelper mItemHelper;

    private SendSmsService mSendSmsService;

    //private SentSmsDataService mSentSmsDataService;

    public ApplySmsRuleCheck(List<BookingDetail> bookingDetails, SmsRule smsRule/*,
                             SentSmsDataService sentSmsDataService*/) {
        this.mBookingDetailList = bookingDetails;
        this.mSmsRule = smsRule;
        mItemHelper = new ApplySmsRuleItemHelper(smsRule.getBookingContainItems(),
                smsRule.getBookingNotContainItems());
        //this.mSentSmsDataService = sentSmsDataService;
        mSendSmsService = new SendSmsServiceImp();
    }

    public void smsRuleCheck() {
        // Log.i("ruleCheck", "called1");
        for (BookingDetail bookingDetail : mBookingDetailList) {
            //   Log.i("ruleCheck", "called2");
            boolean statusCheck = statusIdCheck(bookingDetail);
            // Log.i("ruleCheck", "called3");
            boolean checkInOutStatus = checkCheckInOut(bookingDetail);
            //Log.i("ruleCheck", "called4");
            boolean containNotContainChecker = mItemHelper.getBookingsNeedSms(bookingDetail);
            //Log.i("ruleCheck", "called5");
            //Log.i("statusCheck", String.valueOf(statusCheck));
            //Log.i("checkInStatus", String.valueOf(checkInOutStatus));
            //Log.i("containNot", String.valueOf(containNotContainChecker));
            if(bookingDetail.getId() != null) {
                Log.i("bookingDetailCheck", "" + bookingDetail.getId());
            }
            if(String.valueOf(statusCheck) != null) {
                Log.i("statusCheck", "" + String.valueOf(statusCheck));
            }
            if( String.valueOf(checkInOutStatus) != null) {
                Log.i("checkInOutStatus", "" + String.valueOf(checkInOutStatus));
            }
            if(String.valueOf(containNotContainChecker) != null) {
                Log.i("containNotContain", "" + String.valueOf(containNotContainChecker));
            }
            if (statusCheck && checkInOutStatus && containNotContainChecker) {
                //  Log.i("customerPhone", bookingDetail.getCustomer_phone());


                // Dit is juist
                //mSendSmsService.sendSms(mSmsRule.getSmsToSend(), bookingDetail.getCustomer_phone(),
                //        bookingDetail.getCustomer_name(), bookingDetail.getCustomer_email());

                mSendSmsService.sendSms(mSmsRule.getSmsToSend(),  bookingDetail.getCustomer_phone(), bookingDetail.getCustomer_name(), bookingDetail.getCustomer_email());
                saveCustomerInfo(bookingDetail);
            }


        }
    }

    private void saveCustomerInfo(BookingDetail bookingDetail) {

        CustomerInformation customerInformation = new CustomerInformation();
        customerInformation.setPhoneNumber(bookingDetail.getCustomer_phone());
        customerInformation.setReservationID(bookingDetail.getBooking_id());
        customerInformation.setCustomerName(bookingDetail.getCustomer_name());
        //mSentSmsDataService.saveCustomerInformation(customerInformation, this);
    }

    private boolean statusIdCheck(BookingDetail bookingDetail) {
        // Log.i("mSmsRuleNull", String.valueOf(mSmsRule ==null));
        // Log.i("mBookingStatusNull", String.valueOf(mSmsRule.getmBookingStatus() ==null));
        // Log.i("mBookingDeailStatusNull", String.valueOf(bookingDetail.getStatus_id() ==null));

        if (!(mSmsRule.getmBookingStatus() == null)) {
            if (!bookingDetail.getStatus_id().equals(mSmsRule.getmBookingStatus())) {
                return false;
            }
        }

        return true;

    }

    private boolean checkCheckInOut(BookingDetail bookingDetail) {

        if (!(mSmsRule.getCheckInOutStatus() == 3)) {
            int checkInOutStatus = mSmsRule.getCheckInOutStatus();

            switch (checkInOutStatus) {

                case 0:
                    if (bookingDetail.getCheckin() != 0)
                        return false;
                    break;
                case 1:
                    if (bookingDetail.getCheckin() == 0)
                        return false;
                    break;

                case 2:
                    if (bookingDetail.getCheckout() == 0)
                        return false;
                    break;

            }
        }

        return true;
    }

    @Override
    public void onCustomerInfoSave() {

    }

    @Override
    public void onCustomerInfoSaveFail() {

    }
}
