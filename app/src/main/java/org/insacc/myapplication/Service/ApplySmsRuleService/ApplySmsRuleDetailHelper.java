package org.insacc.myapplication.Service.ApplySmsRuleService;

import android.util.Log;

import org.insacc.myapplication.Database.SentSmsDataService.SentSmsDataService;
import org.insacc.myapplication.Model.Booking;
import org.insacc.myapplication.Model.BookingContainerModel;
import org.insacc.myapplication.Model.BookingDetail;
import org.insacc.myapplication.Model.SmsRule;
import org.insacc.myapplication.Service.GetBookingService.GetBookingService;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Stack;

/**
 * Created by can on 8.02.2017.
 */

public class ApplySmsRuleDetailHelper implements GetBookingService.GetBookingCallback {

    private GetBookingService mGetBookingService;

    private BookingContainerModel mBookingContainerModel;

    //private SentSmsDataService mSentSmsDataService;

    private List<BookingDetail> mBookingDetailList;
    private Stack<Booking> mBookings;

    private SmsRule mSmsRule;
    private ApplySmsRuleCheck mSmsRuleCheck;

    public ApplySmsRuleDetailHelper(BookingContainerModel bookingContainerModel,
                                    GetBookingService getBookingService, SmsRule smsRule/*,
                                    SentSmsDataService sentSmsDataService*/) {
        // Komt hij hier ?
        this.mBookingContainerModel = bookingContainerModel;
        this.mGetBookingService = getBookingService;
        this.mSmsRule = smsRule;
        //this.mSentSmsDataService = sentSmsDataService;
        mBookingDetailList = new ArrayList<>(bookingContainerModel.getBookingIndex().size());
        mBookings = new Stack<>();
    }

    public void getBookingsList() {
        Log.i("getBookingsList", "called");
        for (Map.Entry<String, Booking> entry : mBookingContainerModel.getBookingIndex().entrySet()) {
            mBookings.push(entry.getValue());
        }
        if (mBookings != null && !mBookings.isEmpty() && mBookings.peek() != null)
            mGetBookingService.getBooking(mBookings.pop().getCode(), this);

    }

    @Override
    public void onBookingLoaded(BookingDetail booking) {
        mBookingDetailList.add(booking);
        //Log.i("bookingStack", booking.getItems().get("1").getName());
        if (!mBookings.isEmpty() && mBookings.peek() != null) {
            Log.i("bookingStackPeek", mBookings.peek().getCustomerName());
            mGetBookingService.getBooking(mBookings.pop().getCode(), this);
        } else {
            Log.i("testApply", "called1");
            mSmsRuleCheck = new ApplySmsRuleCheck(mBookingDetailList, mSmsRule/*, mSentSmsDataService*/);
            mSmsRuleCheck.smsRuleCheck();

        }
    }

    @Override
    public void onBookingLoadFailed() {

    }
}
