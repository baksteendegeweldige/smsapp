package org.insacc.myapplication.Service.RebootRecoverService;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.WakefulBroadcastReceiver;
import android.util.Log;

/**
 * Created by can on 13.02.2017.
 */

public class RebootServiceReceiver extends BroadcastReceiver {


    @Override
    public void onReceive(Context context, Intent intent) {
        Log.i("rebootReceiver", "called");
        context.startService(new Intent(context, RebootRecoverService.class));
    }
}
