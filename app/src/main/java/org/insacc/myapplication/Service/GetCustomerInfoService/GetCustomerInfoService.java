package org.insacc.myapplication.Service.GetCustomerInfoService;

import org.insacc.myapplication.Database.BaseService;
import org.insacc.myapplication.Model.Customer;

/**
 * Created by can on 31.01.2017.
 */

public interface GetCustomerInfoService extends BaseService {

    void getCustomerInfo(String customerId, String customerEmail, String customerName,
                         GetCustomerInfoCallback callback);

    interface GetCustomerInfoCallback {
        void onCustomerInfoLoaded(Customer customer);

        void onCustomerInfoLoadFailed();
    }
}
