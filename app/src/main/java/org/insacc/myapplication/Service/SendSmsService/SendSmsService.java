package org.insacc.myapplication.Service.SendSmsService;

import org.insacc.myapplication.Database.BaseService;

/**
 * Created by can on 31.01.2017.
 */

public interface SendSmsService {

    void sendSms(String smsText, String phoneNumber, String customerName, String email);
}
