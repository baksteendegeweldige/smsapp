package org.insacc.myapplication.Service.ApplySmsRuleService;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.WakefulBroadcastReceiver;
import android.util.Log;

import org.insacc.myapplication.Config;

/**
 * Created by can on 31.01.2017.
 */

public class ApplySmsRuleReceiver extends WakefulBroadcastReceiver {


    @Override
    public void onReceive(Context context, Intent intent) {
        Log.i("receiver", "called");
        Intent sendSmsService = new Intent(context, ApplySmsRuleService.class);
        //sendLocService.putExtra(Config.ENTITY_ID, mEntityId);
        sendSmsService.putExtra(Config.SMS_RULE_ID, intent.getExtras().getLong(Config.SMS_RULE_ID));
        startWakefulService(context, sendSmsService);
    }


}
