package org.insacc.myapplication.Service.GetBookingService;

import org.insacc.myapplication.Database.BaseService;
import org.insacc.myapplication.Model.Booking;
import org.insacc.myapplication.Model.BookingDetail;
import org.insacc.myapplication.Model.BookingDetailContainerModel;

/**
 * Created by can on 19.01.2017.
 */

public interface GetBookingService extends BaseService {

    void getBooking(String bookingId, GetBookingCallback callback);

    interface GetBookingCallback {

        void onBookingLoaded(BookingDetail booking);

        void onBookingLoadFailed();
    }
}
