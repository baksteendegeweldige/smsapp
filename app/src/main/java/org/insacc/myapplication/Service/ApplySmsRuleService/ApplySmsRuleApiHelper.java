package org.insacc.myapplication.Service.ApplySmsRuleService;

import org.insacc.myapplication.Database.SentSmsDataService.SentSmsDataService;
import org.insacc.myapplication.Model.BookingContainerModel;
import org.insacc.myapplication.Model.SmsRule;
import org.insacc.myapplication.Service.GetBookingService.GetBookingService;
import org.insacc.myapplication.Service.GetBookingsService.GetBookingsService;

import java.util.HashMap;

import retrofit2.http.QueryMap;
import rx.Subscription;

/**
 * Created by can on 8.02.2017.
 */

public class ApplySmsRuleApiHelper implements GetBookingsService.GetBookingsCallback {


    private GetBookingService mGetBookingService;

    private GetBookingsService mGetBookingsService;

    //private SentSmsDataService mSentSmsDataService;

    private Subscription mSubscription;

    private ApplySmsRuleDetailHelper mDetailHelper;

    private SmsRule mSmsRule;

    public ApplySmsRuleApiHelper(GetBookingsService getBookingsService, GetBookingService getBookingService,
                                 SmsRule smsRule/*, SentSmsDataService sentSmsDataService*/) {

        this.mGetBookingsService = getBookingsService;
        this.mGetBookingService = getBookingService;
        //this.mSentSmsDataService = sentSmsDataService;
        this.mSmsRule = smsRule;


    }

    public void getBookings(HashMap<String, Object> queryMap) {
        mGetBookingsService.getBookings(queryMap, this);
    }

    @Override
    public void onBookingsLoaded(BookingContainerModel bookingContainerModel) {
        mDetailHelper = new ApplySmsRuleDetailHelper(bookingContainerModel, mGetBookingService, mSmsRule/*,
                mSentSmsDataService*/);
        mDetailHelper.getBookingsList();
    }

    @Override
    public void onBookingLoadFail() {

    }
}
