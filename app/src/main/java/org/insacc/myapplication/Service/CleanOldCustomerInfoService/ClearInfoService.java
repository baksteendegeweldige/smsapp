package org.insacc.myapplication.Service.CleanOldCustomerInfoService;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;

import org.insacc.myapplication.Database.DatabaseHelper;

/**
 * Created by can on 13.02.2017.
 */

public class ClearInfoService extends Service implements ClearOldCustomerInfoService.ClearOldDateCallback {

    private ClearOldCustomerInfoService mClearOldInfoService;

    private DatabaseHelper mDbHelper;

    private Intent mIntent;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        mIntent = intent;
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mDbHelper = DatabaseHelper.getInstance(getApplicationContext());
        mClearOldInfoService = new ClearOldCustomerInfoServiceImp(mDbHelper);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        mClearOldInfoService.deleteOldDate(this);

        return START_STICKY;
    }

    @Override
    public void onOldDataCleared() {
        ClearInfoReceiver.completeWakefulIntent(mIntent);
        this.stopSelf();
    }

    @Override
    public void onOldDataClearFail() {
        ClearInfoReceiver.completeWakefulIntent(mIntent);
        this.stopSelf();
    }
}
