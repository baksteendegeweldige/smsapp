package org.insacc.myapplication.Service.CleanOldCustomerInfoService;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.WakefulBroadcastReceiver;

/**
 * Created by can on 13.02.2017.
 */

public class ClearInfoReceiver extends WakefulBroadcastReceiver {


    @Override
    public void onReceive(Context context, Intent intent) {
        startWakefulService(context, new Intent(context, ClearInfoService.class));
    }
}
