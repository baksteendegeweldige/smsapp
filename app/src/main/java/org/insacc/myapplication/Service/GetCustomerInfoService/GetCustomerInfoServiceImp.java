package org.insacc.myapplication.Service.GetCustomerInfoService;

import org.insacc.myapplication.Model.Customer;
import org.insacc.myapplication.Service.ApiCall;

import rx.Observable;
import rx.Observer;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by can on 31.01.2017.
 */

public class GetCustomerInfoServiceImp implements GetCustomerInfoService {

    private ApiCall mApiCall;

    private Subscription mSubscription;

    public GetCustomerInfoServiceImp(ApiCall apiCall) {
        this.mApiCall = apiCall;
    }

    @Override
    public void getCustomerInfo(String customerId, String customerEmail, String customerName, final GetCustomerInfoCallback callback) {

        Observable<Customer> getCustomerInfo = mApiCall.getCustomer(customerId, customerEmail, customerName
                , null)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io());
        mSubscription = getCustomerInfo.subscribe(new Observer<Customer>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {
                callback.onCustomerInfoLoadFailed();
            }

            @Override
            public void onNext(Customer customer) {
                callback.onCustomerInfoLoaded(customer);
            }
        });
    }

    @Override
    public void unSubscribe() {
        if (mSubscription != null && !mSubscription.isUnsubscribed())
            mSubscription.unsubscribe();

    }
}
