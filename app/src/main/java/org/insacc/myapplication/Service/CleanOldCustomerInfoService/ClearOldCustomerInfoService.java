package org.insacc.myapplication.Service.CleanOldCustomerInfoService;

import org.insacc.myapplication.Database.BaseService;

/**
 * Created by can on 13.02.2017.
 */

public interface ClearOldCustomerInfoService extends BaseService {

    void deleteOldDate(ClearOldDateCallback deleteCallback);

    interface ClearOldDateCallback {

        void onOldDataCleared();

        void onOldDataClearFail();
    }

}
