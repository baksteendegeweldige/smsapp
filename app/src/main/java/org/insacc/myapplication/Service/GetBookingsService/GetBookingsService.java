package org.insacc.myapplication.Service.GetBookingsService;

import org.insacc.myapplication.Database.BaseService;
import org.insacc.myapplication.Model.Booking;
import org.insacc.myapplication.Model.BookingContainerModel;

import java.util.HashMap;
import java.util.List;

import retrofit2.http.QueryMap;

/**
 * Created by can on 19.01.2017.
 */

public interface GetBookingsService extends BaseService {
    //TODO add request parameters
    void getBookings(HashMap<String, Object> queryMap, GetBookingsCallback callback);

    interface GetBookingsCallback {

        void onBookingsLoaded(BookingContainerModel bookingContainerModel);

        void onBookingLoadFail();
    }
}
