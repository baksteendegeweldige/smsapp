package org.insacc.myapplication.Service.Interceptors;

import java.io.IOException;

import okhttp3.Credentials;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by can on 2.02.2017.
 */

public class RequestInterceptor implements Interceptor {


    @Override
    public Response intercept(Chain chain) throws IOException {
        Request original = chain.request();
        String token = Credentials.basic("ed14b385fb310f52084b9deee203757723cce455",
                "414f125927c78ec4f360dd447098abd65be310cc3d09718307b944adf86c3f51");
        Request request = original.newBuilder()

                .addHeader("Authorization", token)
                .method(original.method(), original.body())
                .build();

        return chain.proceed(request);

    }
}
