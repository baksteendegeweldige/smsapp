package org.insacc.myapplication.Service.RebootRecoverService;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;

import org.insacc.myapplication.Database.DatabaseHelper;
import org.insacc.myapplication.Database.GetSmsRulesService.GetSmsRulesService;
import org.insacc.myapplication.Database.GetSmsRulesService.GetSmsRulesServiceImp;
import org.insacc.myapplication.Service.RebootRecoverService.RebootRecoverHelperService.RebootRecoverHelperService;
import org.insacc.myapplication.Service.RebootRecoverService.RebootRecoverHelperService.RebootRecoverHelperServiceImp;

/**
 * Created by can on 13.02.2017.
 */

public class RebootRecoverService extends Service implements RebootRecoverHelperService.RebootRecoverHelperCallback {

    private GetSmsRulesService mGetSmsRulesService;

    private RebootRecoverHelperService mRecoverHelperService;

    private DatabaseHelper mDbHelper;

    // private Intent mIntent;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        // this.mIntent = intent;
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mDbHelper = DatabaseHelper.getInstance(getApplicationContext());
        mGetSmsRulesService = new GetSmsRulesServiceImp(mDbHelper);
        mRecoverHelperService = new RebootRecoverHelperServiceImp(mGetSmsRulesService,
                getApplicationContext());

    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        mRecoverHelperService.startAllRulesAfterReboot(this);

        return START_STICKY;
    }

    @Override
    public void onRebootRecoverySucceed() {
        //RebootServiceReceiver.completeWakefulIntent(mIntent);
        mRecoverHelperService.unSubscribe();
        this.stopSelf();
    }

    @Override
    public void onRebootRecoveryFailed() {
        //RebootServiceReceiver.completeWakefulIntent(mIntent);
        mRecoverHelperService.unSubscribe();
        this.stopSelf();
    }


}
