package org.insacc.myapplication.Service.ApplySmsRuleService;

import android.util.Log;

import org.insacc.myapplication.Model.Booking;
import org.insacc.myapplication.Model.BookingContainerModel;
import org.insacc.myapplication.Model.BookingDetail;
import org.insacc.myapplication.Model.BookingSkuItem;
import org.insacc.myapplication.Model.Item;
import org.insacc.myapplication.Model.SmsRule;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by can on 8.02.2017.
 */

public class ApplySmsRuleItemHelper {

    private List<BookingSkuItem> containsItems;

    private List<BookingSkuItem> notContainsItems;

    public ApplySmsRuleItemHelper(List<BookingSkuItem> containsItems, List<BookingSkuItem> notContainsItems) {
        this.containsItems = containsItems;
        this.notContainsItems = notContainsItems;
    }

    public boolean getBookingsNeedSms(BookingDetail bookingDetail) {

        HashMap<String, Boolean> bookingItemNames = getBookingItemsNames(bookingDetail);

        if (bookingContainsItems(bookingItemNames, containsItems) && bookingNotContainsItems(
                bookingItemNames, notContainsItems)) {
            return true;
        }


        return false;
    }

    private boolean bookingContainsItems(HashMap<String, Boolean> itemNames, List<BookingSkuItem> skuItems) {
        if (skuItems == null)
            return true;
        Log.i("bookingContains", skuItems.get(0).getItemName().toString());
        Log.i("bookingContains1", "fenerbahceananinami");
        Log.i("skuItemSize", String.valueOf(skuItems.size()));
        Log.i("itemNameSize", String.valueOf(skuItems.get(0).getItemName().get(0).length() == 0));
        if (skuItems.get(0).getItemName().get(0).length() == 0)
            return true;
        for (BookingSkuItem bookingSkuItem : skuItems) {
            boolean result = false;
            for (String itemName : bookingSkuItem.getItemName()) {
                Log.i("containHashmap", String.valueOf(itemNames.get(itemName)));
                if (!itemNames.containsKey(itemName)) {
                    result = false;
                    break;
                } else {
                    result = true;
                }


            }
            if (result) {
                return true;
            }

        }


        return false;
    }

    private boolean bookingNotContainsItems(HashMap<String, Boolean> itemNames, List<BookingSkuItem> skuItems) {
        if (skuItems == null)
            return true;
        Log.i("bookingNotContains", skuItems.get(0).getItemName().toString());

        if (skuItems.get(0).getItemName().get(0).length() == 0)
            return true;

        for (BookingSkuItem bookingSkuItem : skuItems) {

            boolean result = false;

            for (String itemName : bookingSkuItem.getItemName()) {
                if (itemNames.containsKey(itemName)) {
                    return false;
                } else {
                    result = true;
                }


            }
            if (result) {
                return true;
            }

        }


        return false;
    }

    private HashMap<String, Boolean> getBookingItemsNames(BookingDetail bookingDetail) {
        HashMap<String, Boolean> itemNames = new HashMap<>();

        for (Map.Entry<String, Item> item : bookingDetail.getItems().entrySet()) {
            itemNames.put(item.getValue().getSku(), true);

        }

        return itemNames;
    }
}
