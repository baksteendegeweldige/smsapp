package org.insacc.myapplication.Service;

import org.insacc.myapplication.Model.Booking;
import org.insacc.myapplication.Model.BookingContainerModel;
import org.insacc.myapplication.Model.BookingDetail;
import org.insacc.myapplication.Model.BookingDetailContainerModel;
import org.insacc.myapplication.Model.Customer;
import org.insacc.myapplication.Model.ServerResponse.AccessToken;
import org.insacc.myapplication.Model.ServerResponse.AuthorizeToken;

import java.util.List;
import java.util.Map;

import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;
import rx.Observable;

/**
 * Created by can on 8.01.2017.
 */

public interface ApiCall {

    @GET("booking")
    Observable<BookingContainerModel> getBookings(@QueryMap Map<String, Object> query);

    @GET("booking/{booking_id}")
    Observable<BookingDetail> getBooking(@Path("booking_id") String bookingId);

    @GET("/oauth")
    Observable<AuthorizeToken> getAuthorizeToken(@Query("client_id") String clientId, @Query("response_type")
            String responseType, @Query("redirect_uri") String redirectUrl);

    @GET("customer")
    Observable<Customer> getCustomer(@Query("customer_id") String customerId, @Query("customer_email")
            String customerEmail, @Query("customer_name") String customerName,
                                     @Query("customer_phone") String customerPhone);
}
