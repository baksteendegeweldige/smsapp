package org.insacc.myapplication;

/**
 * Created by can on 8.01.2017.
 */

public class Config {

    public static String BASE_URL = "https://lazertag.checkfront.com/api/3.0/";


    //shared preferences keys
    public static final String ENDPOINT_API = "apiEndpoint";

    public static final String RECEIVED_SMS_NUMBER = "receivedSmsNum";
    public static final String RECEIVED_SMS_TEXT = "receivedSmsText";

    public static final String EMAIL_ADDRESS = "emailAddress";
    public static final String EMAIL_PASSWORD = "emailPassword";

    //Bundle tags
    public static final String IS_CONTAINED_ITEM_DIALOG = "isContainedItemDialog";
    public static final String SMS_RULE_ID = "smsRuleId";

    public static final String EDIT_SMS_RULE = "editSmsRule";


}
