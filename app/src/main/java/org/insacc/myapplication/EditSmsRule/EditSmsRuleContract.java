package org.insacc.myapplication.EditSmsRule;

import org.insacc.myapplication.Model.SmsRule;

/**
 * Created by can on 27.01.2017.
 */

public interface EditSmsRuleContract {

    interface View {
        void populateSmsRule(SmsRule smsRule);

        void openSmsRulesListUi();

        void restartSmsRuleAlarm(SmsRule smsRule);

        void displayRuleUpdatedMsg();

        void displayRuleUpdateFailMsg();


    }

    interface Presenter {
        void loadSmsRule(long ruleId);

        void saveEditedRule(SmsRule smsRule);


    }
}
