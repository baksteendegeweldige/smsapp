package org.insacc.myapplication.EditSmsRule;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.NumberPicker;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TimePicker;

import org.insacc.myapplication.AddSmsRule.InAcitivityItemList.InActivityItemListAdapter;
import org.insacc.myapplication.Config;
import org.insacc.myapplication.DaggerModules.DbHelperModule;
import org.insacc.myapplication.DaggerModules.EditSmsRuleModule.DaggerEditSmsRuleComponent;
import org.insacc.myapplication.DaggerModules.EditSmsRuleModule.EditSmsRuleModule;
import org.insacc.myapplication.Model.BookingSkuItem;
import org.insacc.myapplication.Model.SmsRule;
import org.insacc.myapplication.MyApp;
import org.insacc.myapplication.R;
import org.insacc.myapplication.SmsRules.SmsRuleActivity;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;

/**
 * Created by can on 27.01.2017.
 */

public class EditSmsRuleActivity extends AppCompatActivity implements EditSmsRuleContract.View {

    @Inject
    EditSmsRuleContract.Presenter mPresenter;

    @BindView(R.id.edit_sms_cancel_button)
    ImageButton mCancelButton;

    @BindView(R.id.edit_new_rule_button)
    Button mAddSmsButton;

    @BindView(R.id.edit_rule_name)
    EditText mRuleName;

    @BindView(R.id.edit_current_time_picker)
    TimePicker mCurrentTimePicker;

    @BindView(R.id.edit_plus_day_picker)
    NumberPicker mPlusDayPicker;

    @BindView(R.id.edit_sms_rule_sms_text)
    EditText mSmsText;

    @BindView(R.id.edit_rule_booking_contain_button)
    ImageButton mBookingContainButton;

    @BindView(R.id.edit_rule_booking_not_contain_button)
    ImageButton mBookingNotContainButton;

    @BindView(R.id.edit_rule_select_booking_w_group)
    RadioGroup mSelectBookingWith;

    @BindView(R.id.edit_rule_check_in_status_radio)
    RadioGroup mCheckInStatusGroup;

    //@BindView(R.id.add_rule_user_received_sms_layout)
    //RadioGroup mUserReceivedRadioGroup;

    @BindView(R.id.edit_rule_booking_status_group)
    RadioGroup mBookingStatusGroup;

    @BindView(R.id.edit_rule_booking_status_radio_button)
    RadioButton mBookingCustomStatus;

    @BindView(R.id.edit_rule_contains_list)
    RecyclerView mItemContainList;

    @BindView(R.id.edit_rule_not_contain_list)
    RecyclerView mItemNotContainList;


    private List<BookingSkuItem> selectedContainItems;
    private List<BookingSkuItem> selectedNotContainItems;
    private String selectedBookingStatus;

    private LinearLayoutManager mContainListLayoutManager;
    private LinearLayoutManager mNotContainListLayoutManager;

    private InActivityItemListAdapter mContainListAdapter;
    private InActivityItemListAdapter mNotContainListAdapter;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.edit_sms_rule_layout);
        DaggerEditSmsRuleComponent.builder().editSmsRuleModule(new EditSmsRuleModule(this))
                .appComponent(((MyApp) getApplicationContext()).getAppComponent())
                .dbHelperModule(new DbHelperModule())
                .build().inject(this);

        if (getIntent().getExtras() != null) {
            Bundle bundle = getIntent().getExtras();
            long smsRuleId = bundle.getLong(Config.SMS_RULE_ID);
            mPresenter.loadSmsRule(smsRuleId);
        }
    }

    @Override
    public void populateSmsRule(SmsRule smsRule) {

    }

    @Override
    public void openSmsRulesListUi() {
        startActivity(new Intent(this, SmsRuleActivity.class));
        this.finish();
    }

    @Override
    public void restartSmsRuleAlarm(SmsRule smsRule) {

    }

    @Override
    public void displayRuleUpdatedMsg() {

    }

    @Override
    public void displayRuleUpdateFailMsg() {

    }
}
