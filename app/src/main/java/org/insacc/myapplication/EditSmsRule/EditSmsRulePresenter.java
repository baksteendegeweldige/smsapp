package org.insacc.myapplication.EditSmsRule;

import org.insacc.myapplication.Database.AddSmsRuleService.AddSmsService;
import org.insacc.myapplication.Database.GetSmsRuleService.GetSmsRuleService;
import org.insacc.myapplication.Model.SmsRule;

/**
 * Created by can on 27.01.2017.
 */

public class EditSmsRulePresenter implements EditSmsRuleContract.Presenter, GetSmsRuleService.GetSmsRuleCallback, AddSmsService.UpdateSmsRuleCallback {

    private EditSmsRuleContract.View mView;

    private GetSmsRuleService mGetSmsRuleService;

    private AddSmsService mAddSmsService;

    public EditSmsRulePresenter(EditSmsRuleContract.View view, GetSmsRuleService getSmsRuleService,
                                AddSmsService addSmsService) {
        this.mView = view;
        this.mGetSmsRuleService = getSmsRuleService;
        this.mAddSmsService = addSmsService;
    }


    @Override
    public void loadSmsRule(long ruleId) {
        mGetSmsRuleService.getSmsRule(ruleId, this);
    }

    @Override
    public void saveEditedRule(SmsRule smsRule) {
        mAddSmsService.updateSmsRule(smsRule, this);
    }

    @Override
    public void onSmsRuleLoaded(SmsRule smsRule) {
        mView.populateSmsRule(smsRule);
    }

    @Override
    public void onSmsRuleLoadFailed() {
        //TODO displayLoad Fail msg
    }

    @Override
    public void onSmsRuleUpdated() {
        mView.displayRuleUpdatedMsg();
        mView.openSmsRulesListUi();
    }

    @Override
    public void onSmsRuleUpdateFail() {
        mView.displayRuleUpdateFailMsg();
    }
}
